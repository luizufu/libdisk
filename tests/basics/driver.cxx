// clang-format off
#include <array>

#include <libdisk/array.hxx>

#include <algorithm>
#include <filesystem>
#include <iostream>
#include <cstdio>
/* #include <libdisk/list.hxx> */
#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libdisk/map.hxx>
#include <libdisk/set.hxx>
#include <libdisk/vector.hxx>
/* #include <libdisk/mapset.hxx> */
/* #include <libdisk/mapmap.hxx> */
#include <array>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <optional>
#include <random>
#include <sstream>
#include <cassert>

#define massert(Expr, Msg) massert_impl(#Expr, Expr, __FILE__, __LINE__, Msg)

static void massert_impl(const char* expr_str, bool expr, const char* file,
                         int line, const char* msg)
{
    if(!expr)
    {
        std::cerr << "Assert failed:\t" << msg << "\n"
                  << "Expected:\t" << expr_str << "\n"
                  << "Source:\t\t" << file << ", line " << line << "\n";
        abort();
    }
}

static auto next_probability() -> float
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r(), r(), r(), r(), r(), r()};
    static std::mt19937 gen(seed);
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(gen);
}

void print_label(std::string_view label, size_t n, size_t typesize)
{
    std::cout << "Testing " << label << " (" << n << " = " << n * typesize
              << " bytes)" << std::endl;
}

template<typename Val>
auto message(size_t i, const Val& val) -> std::string
{
    std::ostringstream iss;
    iss << "at value container[" << i << "] = " << val;
    return iss.str();
}

template<typename Key>
static auto gen_test_data(uint64_t n)
{
    std::vector<Key> entries(n);
    std::vector<Key> sample_keys(n / 3);
    /* std::default_random_engine urng(std::random_device{}()); */
    std::default_random_engine urng(2019);

    std::vector<Key> ns(2 * n);
    std::iota(ns.begin(), ns.end(), 0);
    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n; ++i)
    {
        entries[i] = ns[i];
    }

    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n / 3; ++i)
    {
        sample_keys[i] = ns[i];
    }

    return std::make_pair(entries, sample_keys);
}

template<typename Key, typename Value>
static auto gen_test_data(uint64_t n)
{
    std::vector<std::pair<Key, Value>> entries(n);
    std::vector<Key> sample_keys(n / 3);
    /* std::default_random_engine urng(std::random_device{}()); */
    std::default_random_engine urng(2019);

    std::vector<Value> ns(2 * n);
    std::iota(ns.begin(), ns.end(), 0);
    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n; ++i)
    {
        entries[i] = {static_cast<Key>(ns[i]), ns[i]};
    }

    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n / 3; ++i)
    {
        sample_keys[i] = ns[i];
    }

    return std::make_pair(entries, sample_keys);
}

/* template<typename It> */
/* void test_list(It begin1, It end1, It begin2, It end2) */
/* { */
/*     std::filesystem::remove("default_allocator"); */
/*     using list_t = disk::list<typename It::value_type>; */

/*     { */
/*         auto h = list_t::create(); */

/*         massert(h.size == 0, "not empty after creation"); */

/*         std::unordered_map<typename It::value_type, size_t> map; */

/*         for(auto it = begin1; it != end1; ++it) */
/*         { */
/*             ++map[*it]; */
/*         } */

/*         size_t i = 0; */
/*         for(auto it = begin1; it != end1; ++it, ++i) */
/*         { */
/*             list_t::push_back(&h, *it); */
/*         } */

/*         massert(h.size == std::distance(begin1, end1), */
/*                 "do not have the right size after insertion"); */

/*         i = 0; */
/*         for(auto it = begin2; it != end2; ++it, ++i) */
/*         { */
/*             massert(list_t::exists(h, *it), message(i, *it).c_str()); */
/*             massert(list_t::remove(&h, *it), message(i, *it).c_str()); */
/*             --map[*it]; */

/*             if(map[*it] > 0) */
/*             { */
/*                 massert(list_t::exists(h, *it), message(i, *it).c_str()); */
/*             } */
/*             else */
/*             { */
/*                 massert(!list_t::exists(h, *it), message(i, *it).c_str()); */
/*             } */
/*         } */

/*         massert(h.size == 0, "not empty after detetion"); */

/*         list_t::destroy(&h); */
/*     } */
/*     std::filesystem::remove("default_allocator"); */
/* } */

template<typename Key, size_t N, template<size_t> typename Alloc>
void test_array()
{
    std::cout << "Testing array; sizeof(Key) == " << sizeof(Key) << std::endl;

    auto [entries, sample_keys] = gen_test_data<Key>(N);
    std::default_random_engine urng(2019);
    std::vector<uint64_t> indices(entries.size(), 0);
    std::iota(indices.begin(), indices.end(), 0);

    std::filesystem::remove("256_allocator");

    {
        Alloc<256> alloc("256_allocator", 0);
        using Array1 = disk::array<Key, N, Alloc, 256>;
        auto h = Array1::create(&alloc);
        std::array<Key, N> array2;
        assert(h.size == N);

        // test write
        auto it = entries.begin();
        for(uint64_t i: indices)
        {
            Key key = *it;
            ++it;
            Array1::write(h, i, key, &alloc);
            array2[i] = key;
            auto res1 = Array1::read(h, i, alloc);
            auto res2 = array2[i];
            assert(res1 == res2);
        }

        // test iterator
        assert(
            array2.size()
            == std::distance(Array1::begin(h, alloc), Array1::end(h, alloc)));
        auto it1 = Array1::begin(h, alloc);
        auto it2 = array2.begin();
        for(; it2 != array2.end(); ++it1, ++it2)
        {
            assert(*it1 == *it2);
        }

        // test access
        std::shuffle(indices.begin(), indices.end(), urng);
        for(uint64_t i: indices)
        {
            auto res1 = Array1::read(h, i, alloc);
            auto res2 = array2[i];
            assert(res1 == res2);
        }

        Array1::destroy(&h, &alloc);
    }

    std::filesystem::remove("256_allocator");
}

template<typename Key, template<size_t> typename Alloc>
void test_vector(uint64_t n)
{
    std::cout << "Testing vector; sizeof(Key) == " << sizeof(Key) << std::endl;

    auto [entries, sample_keys] = gen_test_data<Key>(n);
    std::default_random_engine urng(2019);
    std::vector<uint64_t> indices(entries.size() / 2, 0);
    std::iota(indices.begin(), indices.end(), 0);

    std::filesystem::remove("256_allocator");

    {
        Alloc<256> alloc("256_allocator", 0);
        using Vec1 = disk::vector<Key, Alloc, 256>;
        auto h = Vec1::create(0, &alloc);
        std::vector<Key> vec2;
        assert(h.size == 0);

        // test insertion
        {
            for(const auto& key: entries)
            {
                Vec1::push_back(&h, key, &alloc);
                vec2.push_back(key);
                auto res1 = Vec1::back(h, alloc);
                auto res2 = vec2.back();
                assert(h.size == vec2.size());
                assert(res1 == res2);
            }
            assert(h.size == vec2.size());
        }

        // test iterator
        {
            {
                assert(vec2.size()
                       == std::distance(Vec1::begin(h, alloc),
                                        Vec1::end(h, alloc)));
                auto it1 = Vec1::begin(h, alloc);
                auto it2 = vec2.begin();
                for(; it2 != vec2.end(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                uint32_t i = vec2.size() / 2;
                auto it1 = Vec1::iterator_at(h, i, alloc);
                auto it2 = vec2.begin() + i;
                for(; it2 != vec2.end(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                assert(vec2.size()
                       == std::distance(Vec1::rbegin(h, alloc),
                                        Vec1::rend(h, alloc)));
                auto it1 = Vec1::rbegin(h, alloc);
                auto it2 = vec2.rbegin();
                for(; it2 != vec2.rend(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                uint32_t i = vec2.size() / 2;
                auto it1 = Vec1::riterator_at(h, i, alloc);
                auto it2 = vec2.rbegin() + (vec2.size() - i - 1);
                for(; it2 != vec2.rend(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }
        }

        // test write iterator
        {
            assert(vec2.size()
                    == std::distance(Vec1::begin(h, &alloc),
                                    Vec1::end(h, &alloc)));

            {
                auto vec3 = vec2;
                std::shuffle(vec3.begin(), vec3.end(), urng);

                auto it1 = Vec1::begin(h, &alloc);
                auto it2 = vec2.begin();
                auto it3 = vec3.begin();
                for(; it2 != vec2.end(); ++it1, ++it2, ++it3)
                {
                    *it1 = *it3;
                    *it2 = *it3;
                }
                it1.flush();

                it1 = Vec1::begin(h, &alloc);
                it2 = vec2.begin();

                for(; it2 != vec2.end(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                auto vec3 = vec2;
                std::shuffle(vec3.begin(), vec3.end(), urng);

                uint32_t i = vec2.size() / 2;
                auto it1 = Vec1::iterator_at(h, i, &alloc);
                auto it2 = vec2.begin() + i;
                auto it3 = vec3.begin() + i;
                for(; it2 != vec2.end(); ++it1, ++it2, ++it3)
                {
                    *it1 = *it3;
                    *it2 = *it3;
                }
                it1.flush();

                it1 = Vec1::iterator_at(h, i, &alloc);
                it2 = vec2.begin() + i;
                for(; it2 != vec2.end(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            assert(vec2.size()
                    == std::distance(Vec1::rbegin(h, alloc),
                                    Vec1::rend(h, alloc)));

            {
                auto vec3 = vec2;
                std::shuffle(vec3.begin(), vec3.end(), urng);

                auto it1 = Vec1::rbegin(h, &alloc);
                auto it2 = vec2.rbegin();
                auto it3 = vec3.rbegin();
                for(; it2 != vec2.rend(); ++it1, ++it2, ++it3)
                {
                    *it1 = *it3;
                    *it2 = *it3;
                }
                it1.flush();

                it1 = Vec1::rbegin(h, &alloc);
                it2 = vec2.rbegin();
                for(; it2 != vec2.rend(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                auto vec3 = vec2;
                std::shuffle(vec3.begin(), vec3.end(), urng);

                uint32_t i = vec2.size() / 2;
                auto it1 = Vec1::riterator_at(h, i, &alloc);
                auto it2 = vec2.rbegin() + (vec2.size() - i - 1);
                auto it3 = vec3.rbegin() + (vec3.size() - i - 1);
                for(; it2 != vec2.rend(); ++it1, ++it2, ++it3)
                {
                    *it1 = *it3;
                    *it2 = *it3;
                }
                it1.flush();

                it1 = Vec1::riterator_at(h, i, &alloc);
                it2 = vec2.rbegin() + (vec2.size() - i - 1);
                for(; it2 != vec2.rend(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }
        }

        // test resize
        {
            size_t half_size = vec2.size() / 2;
            Vec1::resize(&h, half_size, &alloc);
            vec2.resize(half_size);
            assert(h.size == vec2.size());
        }

        // test iterator again
        {
            {
                assert(vec2.size()
                       == std::distance(Vec1::begin(h, alloc),
                                        Vec1::end(h, alloc)));
                auto it1 = Vec1::begin(h, alloc);
                auto it2 = vec2.begin();
                for(; it2 != vec2.end(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                uint32_t i = vec2.size() / 2;
                auto it1 = Vec1::iterator_at(h, i, alloc);
                auto it2 = vec2.begin() + i;
                for(; it2 != vec2.end(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                assert(vec2.size()
                       == std::distance(Vec1::rbegin(h, alloc),
                                        Vec1::rend(h, alloc)));
                auto it1 = Vec1::rbegin(h, alloc);
                auto it2 = vec2.rbegin();
                for(; it2 != vec2.rend(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }

            {
                uint32_t i = vec2.size() / 2;
                auto it1 = Vec1::riterator_at(h, i, alloc);
                auto it2 = vec2.rbegin() + (vec2.size() - i - 1);
                for(; it2 != vec2.rend(); ++it1, ++it2)
                {
                    assert(*it1 == *it2);
                }
            }
        }

        // test access
        {
            auto access_indices = indices;
            std::shuffle(access_indices.begin(), access_indices.end(), urng);
            access_indices.resize(access_indices.size() / 3);
            for(uint64_t i: access_indices)
            {
                auto res1 = Vec1::read(h, i, alloc);
                auto res2 = vec2[i];
                assert(res1 == res2);
            }
        }

        // test write
        {
            auto write_indices = indices;
            std::shuffle(write_indices.begin(), write_indices.end(), urng);
            write_indices.resize(write_indices.size() / 3);
            for(uint64_t i: write_indices)
            {
                Vec1::write(h, i, static_cast<Key>(3), &alloc);
                vec2[i] = static_cast<Key>(3);
                auto res1 = Vec1::read(h, i, alloc);
                auto res2 = vec2[i];
                assert(res1 == res2);
            }
        }

        // test read_n
        {
            uint64_t to_read = h.size / 3;
            uint64_t start = (h.size - to_read) / 2;
            std::vector<Key> tmp(to_read);

            Vec1::read_n(h, start, to_read, tmp.data(), alloc);
            for(uint64_t i = 0; i < to_read; ++i)
            {
                assert(Vec1::read(h, start + i, alloc) == tmp[i]);
            }
        }

        // test read_is
        {
            std::vector<uint64_t> indices(h.size, 0);
            std::iota(indices.begin(), indices.end(), 0);

            uint32_t to_read = h.size / 3;
            std::vector<Key> tmp(to_read);

            Vec1::read_is(h, indices.begin(), indices.begin() + to_read,
                          tmp.data(), false, alloc);
            for(uint64_t i = 0; i < to_read; ++i)
            {
                assert(Vec1::read(h, indices[i], alloc) == tmp[i]);
            }
        }

        // test read_lazy
        {
            std::vector<uint64_t> indices(h.size, 0);
            std::iota(indices.begin(), indices.end(), 0);

            uint32_t to_read = h.size / 3;

            uint64_t i = 0;
            Vec1::read_lazy(
                h, indices[i],
                [&](const Key& key) -> std::optional<uint64_t>
                {
                    assert(Vec1::read(h, indices[i], alloc) == key);
                    ++i;
                    if(i >= to_read)
                    {
                        return std::nullopt;
                    }

                    return indices[i];
                },
                alloc);
        }

        // test write_n
        {
            uint64_t to_write = h.size / 3;
            uint64_t start = (h.size - to_write) / 2;
            auto tmp = entries;
            std::shuffle(tmp.begin(), tmp.end(), urng);

            Vec1::write_n(h, start, to_write, tmp.data(), &alloc);
            for(uint64_t i = 0; i < to_write; ++i)
            {
                assert(Vec1::read(h, start + i, alloc) == tmp[i]);
            }
        }

        // test write_is
        {
            std::vector<uint64_t> indices(h.size, 0);
            std::iota(indices.begin(), indices.end(), 0);
            auto tmp = entries;
            std::shuffle(tmp.begin(), tmp.end(), urng);

            uint32_t to_write = h.size / 3;

            Vec1::write_is(h, indices.begin(), indices.begin() + to_write,
                           tmp.data(), false, &alloc);
            for(uint64_t i = 0; i < to_write; ++i)
            {
                assert(Vec1::read(h, indices[i], alloc) == tmp[i]);
            }
        }

        // test transform_n
        {
            uint64_t to_write = h.size / 3;
            uint64_t start = (h.size - to_write) / 2;

            Vec1::transform_n(
                h, start, to_write,
                [](auto& /*unused*/)
                {
                    return 0;
                },
                &alloc);
            for(uint64_t i = 0; i < to_write; ++i)
            {
                assert(Vec1::read(h, start + i, alloc) == 0);
            }
        }

        // test deletion
        {
            for(uint64_t i = 0; i < vec2.size() / 3; ++i)
            {
                Vec1::pop_back(&h, &alloc);
                vec2.pop_back();
                auto res1 = Vec1::back(h, alloc);
                auto res2 = vec2.back();
                assert(res1 == res2);
            }
            assert(h.size == vec2.size());
        }

        Vec1::destroy(&h, &alloc);
    }

    std::filesystem::remove("256_allocator");
}

template<typename Key, template<size_t> typename Alloc>
void test_set(uint64_t n)
{
    std::cout << "Testing set; sizeof(Key) == " << sizeof(Key) << std::endl;

    auto [entries, sample_keys] = gen_test_data<Key>(n);
    std::filesystem::remove("256_allocator");

    {
        Alloc<256> alloc("256_allocator", 0);
        using Set1 = disk::set<Key, Alloc, std::less<Key>, 256>;
        auto h = Set1::create(&alloc);
        std::set<Key> set2;
        assert(h.size == 0);

        // test insertion
        for(const auto& key: entries)
        {
            Set1::insert(&h, key, &alloc);
            set2.insert(key);
            auto res = Set1::find(h, key, alloc);
            assert(res != Set1::end(h, alloc));
            assert(*res == key);
        }
        assert(h.size == set2.size());

        // test iterator
        assert(set2.size()
               == std::distance(Set1::begin(h, alloc), Set1::end(h, alloc)));
        auto it1 = Set1::begin(h, alloc);
        auto it2 = set2.begin();
        for(; it2 != set2.end(); ++it1, ++it2)
        {
            assert(*it1 == *it2);
        }

        // test search
        for(auto const& key: sample_keys)
        {
            auto res1 = Set1::find(h, key, alloc);
            auto res2 = set2.find(key);

            if(res2 != set2.end())
            {
                assert(res1 != Set1::end(h, alloc));
                assert(*res1 == *res2);
            }
            else
            {
                assert(res1 == Set1::end(h, alloc));
            }
        }

        // test deletion
        for(const auto& key: sample_keys)
        {
            /* std::cout << key << std::endl; */
            auto res1 = Set1::remove(&h, key, &alloc);
            auto res2 = set2.find(key);

            if(res2 != set2.end())
            {
                assert(res1.has_value());
                assert(*res1 == *res2);
                set2.erase(res2);
            }
            else
            {
                assert(!res1.has_value());
            }
        }
        assert(h.size == set2.size());

        Set1::destroy(&h, &alloc);
    }

    std::filesystem::remove("256_allocator");
}

template<typename Key, typename Value, template<size_t> typename Alloc>
void test_map(uint64_t n)
{
    std::cout << "Testing map; sizeof(Key) == " << sizeof(Key)
              << "; sizeof(Value) == " << sizeof(Value) << std::endl;

    auto [entries, sample_keys] = gen_test_data<Key, Value>(n);
    std::filesystem::remove("256_allocator");

    {
        Alloc<256> alloc("256_allocator", 0);
        using Map1 = disk::map<Key, Value, Alloc, std::less<Key>, 256>;
        auto h = Map1::create(&alloc);
        std::map<Key, Value> map2;
        assert(h.size == 0);

        // test insertion
        for(const auto& [key, value]: entries)
        {
            Map1::insert(&h, key, value, &alloc);
            map2.insert({key, value});
            auto res = Map1::find(h, key, alloc);
            assert(res != Map1::end(h, alloc));
            assert(res->second == map2.at(key));
        }
        assert(h.size == map2.size());

        // test iterator
        assert(map2.size()
               == std::distance(Map1::begin(h, alloc), Map1::end(h, alloc)));
        auto it1 = Map1::begin(h, alloc);
        auto it2 = map2.begin();
        for(; it2 != map2.end(); ++it1, ++it2)
        {
            assert(it1->second == it2->second);
        }

        // test search
        for(auto const& key: sample_keys)
        {
            auto res1 = Map1::find(h, key, alloc);
            auto res2 = map2.find(key);

            if(res2 != map2.end())
            {
                assert(res1 != Map1::end(h, alloc));
                assert(res1->second == res2->second);
            }
            else
            {
                assert(res1 == Map1::end(h, alloc));
            }
        }

        // test deletion
        for(const auto& key: sample_keys)
        {
            /* std::cout << key << std::endl; */
            auto res1 = Map1::remove(&h, key, &alloc);
            auto res2 = map2.find(key);

            if(res2 != map2.end())
            {
                assert(res1.has_value());
                assert(*res1 == res2->second);
                map2.erase(res2);
            }
            else
            {
                assert(!res1.has_value());
            }
        }
        assert(h.size == map2.size());

        Map1::destroy(&h, &alloc);
    }

    std::filesystem::remove("256_allocator");
}

/* template<typename It> */
/* void test_mapset(It begin1, It end1, It begin2, It end2) */
/* { */
/*     std::filesystem::remove("default_allocator"); */
/*     using mmap_t = disk::mapset<uint64_t, typename It::value_type>; */

/*     { */
/*         std::set<std::pair<uint64_t, typename It::value_type>> mmap; */

/*         auto h = mmap_t::create(); */

/*         massert(h.size == 0, "not empty after creation"); */

/*         size_t i = 0; */
/*         for(auto it = begin1; it != end1; ++it, ++i) */
/*         { */
/*             mmap_t::insert_value(&h, it->v, *it); */
/*             mmap.insert({it->v, *it}); */
/*             massert(mmap_t::exists(h, it->v, *it), message(i, *it).c_str());
 */
/*         } */

/*         i = 0; */
/*         for(auto it = begin1; it != end1; ++it, ++i) */
/*         { */
/*             massert(mmap_t::exists(h, it->v, *it), message(i, *it).c_str());
 */
/*         } */

/*         massert(h.size == mmap.size(), */
/*                 "do not have the right size after insertion"); */

/*         i = 0; */
/*         for(auto it = begin2; it != end2; ++it, ++i) */
/*         { */
/*             auto has_value_it = mmap.find({it->v, *it}); */

/*             if(has_value_it != mmap.end()) */
/*             { */
/*                 massert(mmap_t::remove_value(&h, it->v, *it), */
/*                         message(i, *it).c_str()); */

/*                 mmap.erase({it->v, *it}); */
/*             } */

/*             else */
/*             { */
/*                 massert(!mmap_t::remove_value(&h, it->v, *it), */
/*                         message(i, *it).c_str()); */
/*             } */

/*             massert(!mmap_t::exists(h, it->v, *it), message(i, *it).c_str());
 */
/*         } */

/*         massert(h.size == mmap.size(), "incorrect size after detetion"); */

/*         mmap_t::destroy(&h); */
/*     } */

/*     std::filesystem::remove("default_allocator"); */
/* } */

/* template<typename It> */
/* void test_mapmap(It begin1, It end1, It begin2, It end2) */
/* { */
/*     std::filesystem::remove("default_allocator"); */
/*     using mmap_t = disk::mapmap<uint64_t, typename It::value_type, uint64_t>;
 */

/*     { */
/*         std::map<std::pair<uint64_t, typename It::value_type>, uint64_t>
 * mmap; */

/*         auto h = mmap_t::create(); */

/*         massert(h.size == 0, "not empty after creation"); */

/*         size_t i = 0; */
/*         for(auto it = begin1; it != end1; ++it, ++i) */
/*         { */
/*             uint64_t label = next_probability() * 100; */
/*             mmap_t::insert_value(&h, it->v, *it, label); */
/*             mmap.insert({{it->v, *it}, label}); */
/*             massert(mmap_t::exists(h, it->v, *it), message(i, *it).c_str());
 */
/*         } */

/*         i = 0; */
/*         for(auto it = begin1; it != end1; ++it, ++i) */
/*         { */
/*             auto label = mmap_t::find(h, it->v, *it); */
/*             massert(label.has_value(), message(i, *it).c_str()); */
/*             massert(*label == mmap.at({it->v, *it}), message(i,
 * *it).c_str()); */
/*         } */

/*         massert(h.size == mmap.size(), */
/*                 "do not have the right size after insertion"); */

/*         i = 0; */
/*         for(auto it = begin2; it != end2; ++it, ++i) */
/*         { */
/*             auto has_value_it = mmap.find({it->v, *it}); */

/*             if(has_value_it != mmap.end()) */
/*             { */
/*                 massert(mmap_t::remove_value(&h, it->v, *it).has_value(),
 * message(i, *it).c_str()); */

/*                 mmap.erase({it->v, *it}); */
/*             } */

/*             else */
/*             { */
/*                 massert(!mmap_t::remove_value(&h, it->v, *it), */
/*                         message(i, *it).c_str()); */
/*             } */

/*             massert(!mmap_t::exists(h, it->v, *it), message(i, *it).c_str());
 */
/*         } */

/*         massert(h.size == mmap.size(), "incorrect size after detetion"); */

/*         mmap_t::destroy(&h); */
/*     } */

/*     std::filesystem::remove("default_allocator"); */
/* } */

auto main() -> int
{
    /* std::cout << std::endl; */
    // test_array<uint32_t, 100000, disk::allocator>();
    test_vector<uint32_t, disk::allocator>(100000);
    // test_set<uint32_t, disk::allocator>(100000);
    // test_map<uint32_t, uint32_t, disk::allocator>(100000);
    // test_map<uint64_t, uint32_t, disk::allocator>(100000);
    // test_map<uint32_t, uint64_t, disk::allocator>(100000);

    // test_array<uint32_t, 100000, disk::buffered_allocator>();
    // test_vector<uint32_t, disk::buffered_allocator>(100000);
    // test_set<uint32_t, disk::buffered_allocator>(100000);
    // test_map<uint32_t, uint32_t, disk::buffered_allocator>(100000);
    // test_map<uint64_t, uint32_t, disk::buffered_allocator>(100000);
    // test_map<uint32_t, uint64_t, disk::buffered_allocator>(100000);
}
