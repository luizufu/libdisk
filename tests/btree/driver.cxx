#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libdisk/map.hxx>
/* #include <libdisk/set.hxx> */

#include <map>
#include <vector>
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <random>
#include <sstream>
#include <cassert>
#include <cmath>
#include <cstdio>

template<typename Key, typename Value>
static auto gen_test_data(uint32_t n)
{
    std::vector<std::pair<Key, Value>> entries(n);
    std::vector<Key> sample_keys(n / 3);
    /* std::default_random_engine urng(std::random_device{}()); */
    std::default_random_engine urng(2019);

    std::vector<Value> ns(2 * n);
    std::iota(ns.begin(), ns.end(), 0);
    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n; ++i)
    {
        entries[i] = {static_cast<Key>(ns[i]), ns[i]};
    }

    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n / 3; ++i)
    {
        sample_keys[i] = ns[i];
    }

    return std::make_pair(entries, sample_keys);
}

template<typename Map1, typename Map2, typename Container, typename Allocator>
void test_insertion(typename Map1::header* h1, Map2* map2,
                    const Container& entries, Allocator* alloc)
{
    uint32_t i = 0;
    for(const auto& [value, key]: entries)
    {
        Map1::insert(h1, key, value, alloc);
        assert(Map1::check_validity(*h1, *alloc));
        map2->insert({key, value});
        auto res = Map1::find(*h1, key, *alloc);
        assert(res != Map1::end(*h1, *alloc));
        assert(res->second == map2->at(key));
        i++;
    }

    assert(h1->size == map2->size());
}

template<typename Map1, typename Map2, typename Allocator>
void test_iterator(const typename Map1::header& h1, const Map2& map2,
                   const Allocator& alloc)
{
    {
        assert(map2.size()
               == std::distance(Map1::begin(h1, alloc), Map1::end(h1, alloc)));

        auto it1 = Map1::begin(h1, alloc);
        auto it2 = map2.begin();
        for(; it2 != map2.end(); ++it1, ++it2)
        {
            assert(it1->second == it2->second);
        }
    }

    {
        assert(
            map2.size()
            == std::distance(Map1::rbegin(h1, alloc), Map1::rend(h1, alloc)));

        auto it1 = Map1::rbegin(h1, alloc);
        auto it2 = map2.rbegin();
        for(; it2 != map2.rend(); ++it1, ++it2)
        {
            assert(it1->second == it2->second);
        }
    }
}

template<typename Map1, typename Map2, typename Container, typename Allocator>
void test_search(const typename Map1::header& h1, const Map2& map2,
                 const Container& keys, const Allocator& alloc)
{
    for(auto const& key: keys)
    {
        auto res1 = Map1::find(h1, key, alloc);
        auto res2 = map2.find(key);

        if(res2 != map2.end())
        {
            assert(res1 != Map1::end(h1, alloc));
            assert(res1->second == res2->second);
        }
        else
        {
            assert(res1 == Map1::end(h1, alloc));
        }
    }
}

template<typename Map1, typename Map2, typename Container, typename Allocator>
void test_deletion(typename Map1::header* h1, Map2* map2, const Container& keys,
                   Allocator* alloc)
{
    uint32_t i = 0;
    for(const auto& key: keys)
    {
        auto res1 = Map1::remove(h1, key, alloc);
        assert(Map1::check_validity(*h1, *alloc));
        auto res2 = map2->find(key);

        if(res2 != map2->end())
        {
            assert(res1.has_value());
            assert(*res1 == res2->second);
            map2->erase(res2);
        }
        else
        {
            assert(!res1.has_value());
        }
        ++i;
    }

    assert(h1->size == map2->size());
}

template<typename Key, typename Value, template<size_t> typename Alloc>
void test_split(std::vector<std::pair<Key, Value>>& entries)
{
    using Map = disk::map<Key, Value, Alloc, std::less<Key>, 256>;

    auto sorted_entries = entries;
    std::sort(sorted_entries.begin(), sorted_entries.end());

    uint32_t dim = std::log10(entries.size()) - 1;
    uint32_t by = std::pow(10, dim > 0 ? dim - 1 : dim);
    for(uint32_t i = 0; i < sorted_entries.size(); i += by)
    {
        std::filesystem::remove("256_allocator_split");
        Alloc<256> alloc("256_allocator_split", 0);

        auto h1 = Map::create(&alloc);
        for(auto e: entries)
        {
            Map::insert(&h1, e.first, e.second, &alloc);
        }

        auto h2 = Map::split(&h1, sorted_entries[i].first, &alloc);

        for(uint32_t j = 0; j < i; ++j)
        {
            auto res1 = Map::find(h1, sorted_entries[j].first, alloc);
            auto res2 = Map::find(h2, sorted_entries[j].first, alloc);
            assert(res1 != Map::end(h1, alloc));
            assert(res2 == Map::end(h2, alloc));
            assert(res1->second == sorted_entries[j].second);
        }

        for(uint32_t j = i; j < sorted_entries.size(); ++j)
        {
            auto res1 = Map::find(h1, sorted_entries[j].first, alloc);
            auto res2 = Map::find(h2, sorted_entries[j].first, alloc);
            assert(res1 == Map::end(h1, alloc));
            assert(res2 != Map::end(h2, alloc));
            assert(res2->second == sorted_entries[j].second);
        }

        {
            auto it1 = Map::begin(h1, alloc);
            auto it2 = Map::begin(h2, alloc);
            auto it3 = sorted_entries.begin();
            while(it3 != sorted_entries.begin() + i)
            {
                assert(it1->first == it3->first);
                assert(it1->second == it3->second);
                ++it1;
                ++it3;
            }

            while(it3 != sorted_entries.end())
            {
                assert(it2->first == it3->first);
                assert(it2->second == it3->second);
                ++it2;
                ++it3;
            }
        }

        {
            auto it1 = Map::rbegin(h1, alloc);
            auto it2 = Map::rbegin(h2, alloc);
            auto it3 = sorted_entries.rbegin();
            uint32_t ii = sorted_entries.size() - i;
            uint32_t nn = 0;
            while(it3 != sorted_entries.rbegin() + ii)
            {
                assert(it2->first == it3->first);
                assert(it2->second == it3->second);
                ++it2;
                ++it3;
                nn++;
            }

            while(it3 != sorted_entries.rend())
            {
                assert(it1->first == it3->first);
                assert(it1->second == it3->second);
                ++it1;
                ++it3;
            }
        }

        Map::destroy(&h1, &alloc);
        Map::destroy(&h2, &alloc);
    }

    std::filesystem::remove("256_allocator_split");
}

template<typename Key, typename Value, template<size_t> typename Alloc>
void test_join(std::vector<std::pair<Key, Value>>& entries)
{
    if(entries.size() == 1)
    {
        return;
    }

    using Map = disk::map<Key, Value, Alloc, std::less<Key>, 256>;

    std::sort(entries.begin(), entries.end());
    uint32_t dim = std::log10(entries.size()) - 1;
    uint32_t by = std::pow(10, dim > 0 ? dim - 1 : dim);
    for(uint32_t n = 0; n <= entries.size(); n += by)
    {
        std::filesystem::remove("256_allocator_join");
        Alloc<256> alloc("256_allocator_join", 0);

        auto h1 = Map::create(&alloc);
        auto h2 = Map::create(&alloc);

        for(uint32_t i = 0; i < n; ++i)
        {
            Map::insert(&h1, entries[i].first, entries[i].second, &alloc);
        }

        for(uint32_t i = n; i < entries.size(); ++i)
        {
            Map::insert(&h2, entries[i].first, entries[i].second, &alloc);
        }

        Map::join(&h1, &h2, &alloc);
        assert(h1.size == entries.size());
        assert(Map::check_validity(h1, alloc));

        uint32_t k = 0;
        for(auto [key, value]: entries)
        {
            auto res = Map::find(h1, key, alloc);
            assert(res != Map::end(h1, alloc));
            assert(res->second == value);
            ++k;
        }

        {
            auto it1 = Map::begin(h1, alloc);
            auto it2 = entries.begin();
            while(it2 != entries.end())
            {
                assert(it1->first == it2->first);
                assert(it1->second == it2->second);
                ++it1;
                ++it2;
            }
        }

        {
            auto it1 = Map::rbegin(h1, alloc);
            auto it2 = entries.rbegin();
            while(it2 != entries.rend())
            {
                assert(it1->first == it2->first);
                assert(it1->second == it2->second);
                ++it1;
                ++it2;
            }
        }

        Map::destroy(&h1, &alloc);
    }

    std::filesystem::remove("256_allocator_join");
}

template<typename Key, typename Value, template<size_t> typename Alloc>
void test(uint32_t n)
{
    std::cout << "Testing sizeof(Key) == " << sizeof(Key)
              << "; sizeof(Value) == " << sizeof(Value) << std::endl;

    auto [entries, sample_keys] = gen_test_data<Key, Value>(n);
    using Map = disk::map<Key, Value, Alloc, std::less<Key>, 256>;
    std::filesystem::remove("256_allocator");
    Alloc<256> alloc("256_allocator", 0);

    {
        /* auto h1 = Map::create(&alloc); */
        /* std::map<Key, Value> map2; */
        /* assert(h1.size == 0); */

        /* test_insertion<Map>(&h1, &map2, entries, &alloc); */
        /* test_iterator<Map>(h1, map2, alloc); */
        /* test_search<Map>(h1, map2, sample_keys, alloc); */
        /* test_deletion<Map>(&h1, &map2, sample_keys, &alloc); */
        /* test_search<Map>(h1, map2, sample_keys, alloc); */

        /* Map::destroy(&h1, &alloc); */
        test_split<Key, Value, Alloc>(entries);
        /* test_join<Key, Value, Alloc>(entries); */
    }

    std::filesystem::remove("256_allocator");
}

auto main() -> int
{
    for(uint32_t i = 5; i < 100000; ++i)
    {
        std::cout << i << std::endl;
        test<uint32_t, uint32_t, disk::allocator>(i);
    }

    test<uint32_t, uint32_t, disk::allocator>(30);
    test<uint32_t, uint32_t, disk::buffered_allocator>(30);
    /* test<uint32_t, uint32_t>(588); */
    /* test<uint32_t, uint32_t>(10000); */
    /* test<uint32_t, uint64_t>(10000); */
    /* test<uint64_t, uint32_t>(10000); */
}
