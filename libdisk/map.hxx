#pragma once

#include <array>
#include <queue>
#include <stack>
#include <optional>

namespace disk
{
template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp = std::less<Key>, size_t PAGE_SIZE = 4096>
class map
{
public:
    struct header
    {
        uint64_t root = 0;
        uint64_t first_leaf = 0;
        uint64_t last_leaf = 0;
        uint64_t size = 0;
        uint32_t height = 0;
    };

    struct inner_node;
    struct leaf_node;

    enum node_type : uint32_t
    {
        inner,
        leaf,
    };
    union node
    {
        struct
        {
            union
            {
                inner_node inner;
                leaf_node leaf;
            };
            node_type type;
        };
        std::byte bytes[PAGE_SIZE] = {};
    };

    struct inner_node
    {
        static constexpr uint32_t N_KEYS = (PAGE_SIZE - 16) / (sizeof(Key) + 8);

        void insert(uint32_t i, const Key& key, uint64_t pointer);
        auto remove(uint32_t i) -> uint64_t;
        auto split(inner_node* s) -> Key;
        void share_to(inner_node* s, Key* p_key, uint32_t to_pass,
                      bool no_merge = false);
        void share_from(inner_node* s, Key* p_key, uint32_t to_pass,
                        bool no_merge = false);

        std::array<Key, N_KEYS> keys = {};
        std::array<uint64_t, N_KEYS + 1> pointers = {};
        uint32_t size = 0;
    } __attribute__((packed));

    struct leaf_node
    {
        static constexpr uint32_t N_KEYS =
            (PAGE_SIZE - 24) / (sizeof(Key) + sizeof(Value));

        void insert(uint32_t i, const Key& key, const Value& value);
        auto remove(uint32_t i) -> Value;
        auto split(leaf_node* s, uint64_t pointer1, uint64_t pointer2) -> Key;
        void share_to(leaf_node* s, Key* p_key, uint32_t to_pass,
                      bool no_merge = false);
        void share_from(leaf_node* s, Key* p_key, uint32_t to_pass,
                        bool no_merge = false);

        std::array<Key, N_KEYS> keys = {};
        std::array<Value, N_KEYS> values = {};
        uint64_t next = 0;
        uint64_t prev = 0;
        uint32_t size = 0;
    } __attribute__((packed));

    using context = std::stack<std::pair<uint64_t, uint32_t>>;

    class const_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = std::pair<Key, Value>;
        using reference = value_type;
        struct pointer
        {
            value_type t;
            auto operator->() -> value_type*
            {
                return &t;
            }
        };
        using iterator_category = std::forward_iterator_tag;

        const_iterator(uint64_t leaf, uint32_t i,
                       const Alloc<PAGE_SIZE>& alloc);

        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_iterator&;
        auto operator++(int) -> const_iterator;
        auto operator==(const const_iterator& other) const -> bool;
        auto operator!=(const const_iterator& other) const -> bool;

    private:
        node _n;
        uint64_t _id;
        uint32_t _i;
        const Alloc<PAGE_SIZE>& _alloc;
    };

    class const_reverse_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = std::pair<Key, Value>;
        using reference = value_type;
        struct pointer
        {
            value_type t;
            auto operator->() -> value_type*
            {
                return &t;
            }
        };
        using iterator_category = std::forward_iterator_tag;

        const_reverse_iterator(uint64_t leaf, uint32_t i,
                               const Alloc<PAGE_SIZE>& alloc);

        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_reverse_iterator&;
        auto operator++(int) -> const_reverse_iterator;
        auto operator==(const const_reverse_iterator& other) const -> bool;
        auto operator!=(const const_reverse_iterator& other) const -> bool;

    private:
        node _n;
        uint64_t _id;
        uint32_t _i;
        const Alloc<PAGE_SIZE>& _alloc;
    };

    static auto create(Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void destroy_lazy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void clear_lazy(header* h, Alloc<PAGE_SIZE>* alloc);

    static auto insert(header* h, const Key& key, const Value& value,
                       Alloc<PAGE_SIZE>* alloc) -> bool;
    static auto insert_or_update(header* h, const Key& key, const Value& value,
                                 Alloc<PAGE_SIZE>* alloc)
        -> std::optional<Value>;

    static auto remove(header* h, const Key& key, Alloc<PAGE_SIZE>* alloc)
        -> std::optional<Value>;

    static void join(header* h1, header* h2, Alloc<PAGE_SIZE>* alloc);

    static auto split(header* h1, const Key& key, Alloc<PAGE_SIZE>* alloc)
        -> header;

    static auto
    check_validity(const header& h,
                   const Alloc<PAGE_SIZE>& alloc = *default_Alloc<PAGE_SIZE>())
        -> bool
    {
        node n = {};
        alloc.pread(h.root, n.bytes);
        if(n.type == node_type::inner)
        {
            std::queue<uint64_t> nodes;
            for(uint64_t i = 0; i < n.inner.size + 1; ++i)
            {
                nodes.push(n.inner.pointers[i]);
            }

            while(!nodes.empty())
            {
                alloc.pread(nodes.front(), n.bytes);
                nodes.pop();

                if(n.type == node_type::leaf)
                {
                    if(n.leaf.size < leaf_node::N_KEYS / 2 - 1)
                    {
                        return false;
                    }
                }
                else
                {
                    if(n.inner.size < inner_node::N_KEYS / 2 - 1)
                    {
                        return false;
                    }

                    for(uint64_t i = 0; i < n.inner.size + 1; ++i)
                    {
                        nodes.push(n.inner.pointers[i]);
                    }
                }
            }
        }

        return true;
    }

    static auto find(const header& h, const Key& key,
                     const Alloc<PAGE_SIZE>& alloc) -> const_iterator;

    static auto begin(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;
    static auto end(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;

    static auto rbegin(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_reverse_iterator;
    static auto rend(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_reverse_iterator;

    template<typename CompareFunc = Comp>
    static auto lower_bound(const header& h, const Key& key, CompareFunc comp,
                            const Alloc<PAGE_SIZE>& alloc) -> const_iterator;

    template<typename CompareFunc = Comp>
    static auto upper_bound(const header& h, const Key& key, CompareFunc comp,
                            const Alloc<PAGE_SIZE>& alloc) -> const_iterator;

    template<typename CompareFunc>
    static auto find_leaf(const header& h, const Key& key, CompareFunc comp,
                          const Alloc<PAGE_SIZE>& alloc) -> context;

private:
    static auto insert_inner(context* ctx, const Key& key, uint64_t pointer,
                             Alloc<PAGE_SIZE>* alloc) -> std::optional<node>;
    static auto remove_inner(context* ctx, node&& n, uint64_t n_id,
                             uint32_t n_i, Alloc<PAGE_SIZE>* alloc)
        -> std::optional<uint64_t>;

    template<typename CompareFunc>
    static auto find_leaf(uint64_t n_id, const Key& key, CompareFunc comp,
                          const Alloc<PAGE_SIZE>& alloc) -> context;

    static auto find_first(uint64_t n_id, uint32_t max_depth,
                           const Alloc<PAGE_SIZE>& alloc) -> context;

    static auto find_last(uint64_t n_id, uint32_t max_depth,
                          const Alloc<PAGE_SIZE>& alloc) -> context;

    static auto join_nodes(uint64_t n1_id, uint64_t n2_id, uint32_t* n1_height,
                           uint32_t n2_height, Key n2_smallest,
                           uint64_t* n1_first_leaf, uint64_t* n1_last_leaf,
                           std::pair<uint64_t*, uint64_t*>* n2_leaves,
                           Alloc<PAGE_SIZE>* alloc) -> uint64_t;
};

} // namespace disk

#include <libdisk/map-iterator.ixx>
#include <libdisk/map-node.ixx>
#include <libdisk/map.ixx>
