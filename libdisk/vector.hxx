#pragma once

#include <array>

namespace disk
{
template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class vector
{
    static const uint32_t N_ELEMENTS = PAGE_SIZE / sizeof(T);

public:
    union block
    {
        std::array<T, N_ELEMENTS> elements;
        std::byte bytes[PAGE_SIZE];
    };

    struct header
    {
        uint64_t head = 0;
        uint64_t size = 0;
        uint64_t capacity = 0;
    };

    class iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using reference = value_type&;
        using pointer = value_type*;
        using iterator_category = std::forward_iterator_tag;

        iterator(uint64_t head, uint64_t i, Alloc<PAGE_SIZE>* alloc);

        auto operator*() -> reference;
        auto operator->() -> pointer;
        auto operator++() -> iterator&;
        auto operator++(int) -> iterator;
        auto operator==(const iterator& other) const -> bool;
        auto operator!=(const iterator& other) const -> bool;
        void flush();

    private:
        block _b;
        uint64_t _head;
        uint64_t _i;
        Alloc<PAGE_SIZE>* _alloc;
    };

    class reverse_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using reference = value_type&;
        using pointer = value_type*;
        using iterator_category = std::forward_iterator_tag;

        reverse_iterator(uint64_t head, uint64_t i, Alloc<PAGE_SIZE>* alloc);

        auto operator*() -> reference;
        auto operator->() -> pointer;
        auto operator++() -> reverse_iterator&;
        auto operator++(int) -> reverse_iterator;
        auto operator==(const reverse_iterator& other) const -> bool;
        auto operator!=(const reverse_iterator& other) const -> bool;
        void flush();

    private:
        block _b;
        uint64_t _head;
        uint64_t _i;
        Alloc<PAGE_SIZE>* _alloc;
    };

    class const_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using reference = const value_type&;
        using pointer = const value_type*;
        using iterator_category = std::forward_iterator_tag;

        const_iterator(uint64_t head, uint64_t i,
                       const Alloc<PAGE_SIZE>& alloc);

        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_iterator&;
        auto operator++(int) -> const_iterator;
        auto operator==(const const_iterator& other) const -> bool;
        auto operator!=(const const_iterator& other) const -> bool;

    private:
        block _b;
        uint64_t _head;
        uint64_t _i;
        const Alloc<PAGE_SIZE>& _alloc;
    };

    class const_reverse_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using reference = const value_type&;
        using pointer = const value_type*;
        using iterator_category = std::forward_iterator_tag;

        const_reverse_iterator(uint64_t head, uint64_t i,
                               const Alloc<PAGE_SIZE>& alloc);

        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_reverse_iterator&;
        auto operator++(int) -> const_reverse_iterator;
        auto operator==(const const_reverse_iterator& other) const -> bool;
        auto operator!=(const const_reverse_iterator& other) const -> bool;

    private:
        block _b;
        uint64_t _head;
        uint64_t _i;
        const Alloc<PAGE_SIZE>& _alloc;
    };

    static auto create(uint64_t n, Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static auto read(const header& h, uint64_t i, const Alloc<PAGE_SIZE>& alloc)
        -> T;
    static void read_n(const header& h, uint64_t i, uint64_t n, T* out,
                       const Alloc<PAGE_SIZE>& alloc);
    template<typename Iter>
    static void read_is(const header& h, Iter it, Iter end, T* out,
                        bool use_i_on_out, const Alloc<PAGE_SIZE>& alloc,
                        uint64_t initial_pos = 0);
    template<typename Func>
    static void read_lazy(const header& h, uint64_t i, Func next_is,
                          const Alloc<PAGE_SIZE>& alloc);

    static void write(const header& h, uint64_t i, const T& elem,
                      Alloc<PAGE_SIZE>* alloc);
    static void write_n(const header& h, uint64_t i, uint64_t n, const T* in,
                        Alloc<PAGE_SIZE>* alloc);
    template<typename Iter>
    static void write_is(const header& h, Iter it, Iter end, T* in,
                         bool use_i_on_in, Alloc<PAGE_SIZE>* alloc,
                         uint64_t initial_pos = 0);
    template<typename Func>
    static void write_lazy(const header& h, uint64_t i, Func next_is,
                           Alloc<PAGE_SIZE>* alloc);

    static void fill(const header& h, const T& elem, Alloc<PAGE_SIZE>* alloc);
    template<typename Function>
    static void transform_n(const header& h, uint64_t i, uint64_t n, Function f,
                            Alloc<PAGE_SIZE>* alloc);

    static void push_back(header* h, const T& elem, Alloc<PAGE_SIZE>* alloc);
    static void pop_back(header* h, Alloc<PAGE_SIZE>* alloc);

    static auto
    back(const header& h,
         const Alloc<PAGE_SIZE>& alloc = *default_Alloc<PAGE_SIZE>()) -> T;

    static void resize(header* h, uint64_t n, Alloc<PAGE_SIZE>* alloc);
    static void reserve(header* h, uint64_t n, Alloc<PAGE_SIZE>* alloc);
    static void shrink_to_fit(header* h, Alloc<PAGE_SIZE>* alloc);

    static auto begin(const header& h, Alloc<PAGE_SIZE>* alloc) -> iterator;
    static auto iterator_at(const header& h, uint64_t i,
                            Alloc<PAGE_SIZE>* alloc) -> iterator;
    static auto end(const header& h, Alloc<PAGE_SIZE>* alloc) -> iterator;

    static auto rbegin(const header& h, Alloc<PAGE_SIZE>* alloc)
        -> reverse_iterator;
    static auto riterator_at(const header& h, uint64_t i,
                             Alloc<PAGE_SIZE>* alloc) -> reverse_iterator;
    static auto rend(const header& h, Alloc<PAGE_SIZE>* alloc)
        -> reverse_iterator;

    static auto begin(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;
    static auto iterator_at(const header& h, uint64_t i,
                            const Alloc<PAGE_SIZE>& alloc) -> const_iterator;
    static auto end(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;

    static auto rbegin(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_reverse_iterator;
    static auto riterator_at(const header& h, uint64_t i,
                             const Alloc<PAGE_SIZE>& alloc)
        -> const_reverse_iterator;
    static auto rend(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_reverse_iterator;
};

} // namespace disk

#include <libdisk/vector-iterator.ixx>
#include <libdisk/vector-reverse-iterator.ixx>
#include <libdisk/vector.ixx>
