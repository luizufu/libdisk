#pragma once

#include <libcache/lru.hxx>
#include <libdisk/detail/free-lists.hxx>

#include <map>
#include <queue>
#include <vector>
#include <fcntl.h>
#include <memory>

namespace disk
{
template<size_t PAGE_SIZE = 4096>
class allocator
{
    static const int flags = O_CREAT | O_RDWR | O_CLOEXEC;
    static const int mode = 0666;

    union alignas(PAGE_SIZE) page
    {
        struct // header
        {
            mutable uint64_t sequential_accesses;
            mutable uint64_t sequential_writes;
            mutable uint64_t random_accesses;
            mutable uint64_t random_writes;
            detail::free_lists<(PAGE_SIZE - 60) / 8> flists;
            uint64_t pages;
            uint64_t pages_capacity;
            mutable uint64_t last_i;
        };

        uint64_t next;              // deleted page
        std::byte bytes[PAGE_SIZE]; // general page
    };

    using extract_func = std::vector<uint64_t> (*)(uint64_t, allocator* alloc);

    page _header;
    mutable std::unique_ptr<cache::lru> _cache;
    bool _has_cache;
    std::map<uint32_t, std::queue<std::pair<uint64_t, extract_func>>>
        _lazy_free_map;
    int _fd;

public:
    explicit allocator(const char* filename, size_t cache_pages = 16384);
    ~allocator() noexcept;

    allocator(const allocator& other) = delete;
    allocator(allocator&& other) = delete;
    auto operator=(const allocator& other) -> allocator& = delete;
    auto operator=(allocator&& other) -> allocator& = delete;

    auto pnew(uint64_t size = 1) -> uint64_t;
    void pdelete(uint64_t root, uint64_t size = 1);
    void pdelete_lazy(uint64_t root, extract_func f, uint64_t size = 1);
    void prenew(uint64_t* root, uint64_t size_before, uint64_t size_after);
    void pread(uint64_t i, std::byte* page) const;
    void pwrite(uint64_t i, const std::byte* page);
    void clear();

    auto pages() const -> uint64_t;
    auto size() const -> uint64_t;
    auto empty() const -> bool;

    auto cache() const -> const cache::lru&;
    auto sequential_accesses() const -> uint64_t;
    auto sequential_writes() const -> uint64_t;
    auto random_accesses() const -> uint64_t;
    auto random_writes() const -> uint64_t;
    void reset_benchmark() const;
};

} // namespace disk

#include <libdisk/allocator.ixx>
