#pragma once

#include <stack>
#include <optional>

namespace disk
{
template<typename Key, template<size_t> typename Alloc,
         typename Comp = std::less<Key>, size_t PAGE_SIZE = 4096>
class set
{
public:
    struct header
    {
        uint64_t root = 0;
        uint64_t leaf = 0;
        uint64_t size = 0;
    };

    struct inner_node;
    struct leaf_node;

    enum node_type : uint32_t
    {
        inner,
        leaf,
    };

    union node
    {
        struct
        {
            union
            {
                inner_node inner;
                leaf_node leaf;
            };
            node_type type;
        };
        std::byte bytes[PAGE_SIZE] = {};
    };

    struct inner_node
    {
        static constexpr uint32_t N_KEYS = (PAGE_SIZE - 16) / (sizeof(Key) + 8);

        void insert(uint32_t i, const Key& key, uint64_t pointer);
        auto remove(uint32_t i) -> uint64_t;
        auto split(inner_node* s) -> Key;
        void share_left(inner_node* s, Key* p_key);
        void share_right(inner_node* s, Key* p_key);
        void merge(inner_node* s);

        std::array<Key, N_KEYS> keys = {};
        std::array<uint64_t, N_KEYS + 1> pointers = {};
        uint32_t size = 0;
    } __attribute__((packed));

    struct leaf_node
    {
        static constexpr uint32_t N_KEYS = (PAGE_SIZE - 16) / sizeof(Key);

        void insert(uint32_t i, const Key& key);
        auto remove(uint32_t i) -> Key;
        auto split(leaf_node* s, uint64_t as) -> Key;
        void share_left(leaf_node* s, Key* p_key);
        void share_right(leaf_node* s, Key* p_key);
        void merge(leaf_node* s);

        std::array<Key, N_KEYS> keys = {};
        uint64_t next = 0;
        uint32_t size = 0;
    } __attribute__((packed));

    using context = std::stack<std::pair<uint64_t, uint32_t>>;

    class const_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = Key;
        using reference = const value_type&;
        using pointer = const value_type*;
        using iterator_category = std::forward_iterator_tag;

        const_iterator(uint64_t leaf, uint32_t i,
                       const Alloc<PAGE_SIZE>& alloc);

        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_iterator&;
        auto operator++(int) -> const_iterator;
        auto operator==(const const_iterator& other) const -> bool;
        auto operator!=(const const_iterator& other) const -> bool;

    private:
        node _n;
        uint64_t _id;
        uint32_t _i;
        const Alloc<PAGE_SIZE>& _alloc;
    };

    static auto create(Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static auto insert(header* h, const Key& key, Alloc<PAGE_SIZE>* alloc)
        -> bool;
    static auto insert_or_update(header* h, const Key& key,
                                 Alloc<PAGE_SIZE>* alloc) -> std::optional<Key>;

    static auto remove(header* h, const Key& key, Alloc<PAGE_SIZE>* alloc)
        -> std::optional<Key>;

    static auto find(const header& h, const Key& key,
                     const Alloc<PAGE_SIZE>& alloc) -> const_iterator;

    template<typename CompareFunc>
    static auto find_leaf(const header& h, const Key& key, CompareFunc comp,
                          const Alloc<PAGE_SIZE>& alloc) -> context;

    static auto begin(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;
    static auto end(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;

    template<typename CompareFunc = Comp>
    static auto lower_bound(const header& h, const Key& key, CompareFunc comp,
                            const Alloc<PAGE_SIZE>& alloc) -> const_iterator;

    template<typename CompareFunc = Comp>
    static auto upper_bound(const header& h, const Key& key, CompareFunc comp,
                            const Alloc<PAGE_SIZE>& alloc) -> const_iterator;

private:
    static void insert_inner(header* h, context* ctx, const Key& key,
                             uint64_t pointer, Alloc<PAGE_SIZE>* alloc);
    static void remove_inner(header* h, context* ctx, node&& n, uint64_t n_id,
                             uint32_t n_i, Alloc<PAGE_SIZE>* alloc);
};

} // namespace disk

#include <libdisk/set-iterator.ixx>
#include <libdisk/set-node.ixx>
#include <libdisk/set.ixx>
