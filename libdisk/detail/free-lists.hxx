#pragma once

#include <algorithm>

namespace disk::detail
{
template<size_t MAX_ALLOC_SIZE>
struct free_lists
{
    using flist_entry = std::pair<uint32_t, uint32_t>;

    flist_entry roots[MAX_ALLOC_SIZE];
    uint32_t size = {};
};

template<typename List>
auto flbegin(List* fl)
{
    return fl->roots;
}

template<typename List>
auto flend(List* fl)
{
    return fl->roots + fl->size;
}

template<typename List>
auto fllb(List* fl, uint32_t key)
{
    return std::lower_bound(flbegin(fl), flend(fl), std::make_pair(key, 0U));
}

template<typename List, typename Entry>
void flins(List* fl, Entry* it, Entry entry)
{
    std::copy(it, flend(fl), it + 1);
    *(it) = entry;
    ++fl->size;
}

template<typename List, typename Entry>
void flrem(List* fl, Entry* it)
{
    std::copy(it + 1, flend(fl), it);
    --fl->size;
}

template<typename List>
void flclr(List* fl)
{
    std::fill(flbegin(fl), flend(fl), std::make_pair(0U, 0U));
    fl->size = 0;
}

} // namespace disk::detail
