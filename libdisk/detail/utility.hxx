#pragma once

#include <algorithm>
#include <cstdint>
#include <limits>

namespace disk::detail
{

template<typename Node>
auto kbegin(Node* n)
{
    return n->keys.begin();
}

template<typename Node>
auto kend(Node* n)
{
    return n->keys.begin() + n->size;
}

template<typename Node>
auto vbegin(Node* n)
{
    return n->values.begin();
}

template<typename Node>
auto vend(Node* n)
{
    return n->values.begin() + n->size;
}

template<typename Node>
auto pbegin(Node* n)
{
    return n->pointers.begin();
}

template<typename Node>
auto pend(Node* n)
{
    return n->pointers.begin() + n->size + 1;
}

template<typename Node, typename Key, typename CompareFunc>
auto lb(const Node& n, const Key& key, CompareFunc comp) -> uint32_t
{
    return std::lower_bound(kbegin(&n), kend(&n), key, comp) - kbegin(&n);
}

template<typename Node, typename Key, typename CompareFunc>
auto ub(const Node& n, const Key& key, CompareFunc comp) -> uint32_t
{
    return std::upper_bound(kbegin(&n), kend(&n), key, comp) - kbegin(&n);
}



constexpr auto ceil(float num) -> uint32_t
{
    return (static_cast<float>(static_cast<uint32_t>(num)) == num)
               ? static_cast<uint32_t>(num)
               : static_cast<uint32_t>(num) + ((num > 0) ? 1 : 0);
}

template<typename T>
auto find_ord_eq(T* begin, T* end, const T& key) -> T*
{
    auto it = std::lower_bound(begin, end, key);

    if(it != end && !(*it < key) && !(key < *it))
    {
        return it;
    }

    return end;
}

template<typename T, typename Comp>
auto find_ord_eq(T* begin, T* end, const T& key, Comp comp) -> T*
{
    auto it = std::lower_bound(begin, end, key, comp);

    if(it != end && !comp(*it, key) && !comp(key, *it))
    {
        return it;
    }

    return end;
}

template<typename T>
auto find_ord_gt(T* begin, T* end, const T& key) -> T*
{
    return std::upper_bound(begin, end, key);
}

template<typename T, typename Comp>
auto find_ord_gt(T* begin, T* end, const T& key, Comp comp) -> T*
{
    return std::upper_bound(begin, end, key, comp);
}

template<typename T>
auto find_ord_geq(T* begin, T* end, const T& key) -> T*
{
    return std::lower_bound(begin, end, key);
}

// container should be larger than end - begin or last element will be lost
template<typename T>
void insert(T* begin, T* end, size_t i, const T& key)
{
    std::copy_backward(begin + i, end, end + 1);
    *(begin + i) = key;
}

// allocated space should be larger than n or last element will be lost
template<typename T, typename Size>
void insert_ord(T* begin, Size& n, const T& key)
{
    T* it = find_ord_gt(begin, begin + n, key);
    insert(begin, begin + n, it - begin, key);
    ++n;
}

template<typename T, typename Size, typename Comp>
void insert_ord(T* begin, Size& n, const T& key, Comp comp)
{
    T* it = find_ord_gt(begin, begin + n, key, comp);
    insert(begin, begin + n, it - begin, key);
    ++n;
}

// container will have the same size
template<typename T>
void remove(T* begin, T* end, size_t i)
{
    std::copy(begin + i + 1, end, begin + i);
}

template<typename T, typename Size>
void remove(T* begin, Size& n, size_t i)
{
    remove(begin, begin + n, i);
    --n;
}

} // namespace disk::detail
