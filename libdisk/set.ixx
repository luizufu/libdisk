#include <libdisk/detail/utility.hxx>

#include <algorithm>

namespace disk
{
template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::create(Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = {};
    h.root = alloc->pnew();
    h.leaf = h.root;
    h.size = 0;

    node root = {};
    root.leaf = leaf_node {};
    root.type = node_type::leaf;

    alloc->pwrite(h.root, root.bytes);

    return h;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::destroy(header* h,
                                               Alloc<PAGE_SIZE>* alloc)
{
    node n = {};
    alloc->pread(h->root, n.bytes);

    if(n.type == node_type::inner)
    {
        header h2 = {};
        for(uint32_t i = 0; i < n.inner.size + 1; ++i)
        {
            h2.root = n.inner.pointers[i];
            destroy(&h2, alloc);
        }
    }

    alloc->pdelete(h->root);
    h->root = 0;
    h->leaf = 0;
    h->size = 0;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::clear(header* h, Alloc<PAGE_SIZE>* alloc)
{
    node n = {};
    alloc->pread(h->root, n.bytes);

    if(n.type == node_type::inner)
    {
        header h2 = {};
        for(uint32_t i = 0; i < n.inner.size + 1; ++i)
        {
            h2.root = n.inner.pointers[i];
            destroy(&h2, alloc);
        }
    }

    n.leaf = leaf_node {};
    n.type = node_type::leaf;
    alloc->pwrite(h->root, n.bytes);
    h->leaf = h->root;
    h->size = 0;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::insert(header* h, const Key& key,
                                              Alloc<PAGE_SIZE>* alloc) -> bool
{
    Comp comp;
    auto ctx = find_leaf(*h, key, comp, *alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    ctx.pop();
    alloc->pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    if(i != n.leaf.size && n.leaf.keys[i] == key)
    {
        return false;
    }

    n.leaf.insert(i, key);
    ++h->size;

    if(n.leaf.size >= leaf_node::N_KEYS)
    {
        node sibling = {};
        sibling.leaf = leaf_node {};
        sibling.type = node_type::leaf;

        uint64_t sibling_id = alloc->pnew();
        Key pkey = n.leaf.split(&sibling.leaf, sibling_id);
        insert_inner(h, &ctx, pkey, sibling_id, alloc);

        alloc->pwrite(sibling_id, sibling.bytes);
    }

    alloc->pwrite(n_id, n.bytes);
    return true;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::insert_or_update(header* h,
                                                        const Key& key,
                                                        Alloc<PAGE_SIZE>* alloc)
    -> std::optional<Key>
{
    Comp comp;
    auto ctx = find_leaf(*h, key, comp, *alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    ctx.pop();
    alloc->pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    if(i != n.leaf.size && n.leaf.keys[i] == key)
    {
        Key old = n.leaf.keys[i];
        n.leaf.keys[i] = key;
        alloc->pwrite(n_id, n.bytes);
        return old;
    }

    n.leaf.insert(i, key);
    ++h->size;

    if(n.leaf.size >= leaf_node::N_KEYS)
    {
        node sibling = {};
        sibling.leaf = leaf_node {};
        sibling.type = node_type::leaf;

        uint64_t sibling_id = alloc->pnew();
        Key pkey = n.leaf.split(&sibling.leaf, sibling_id);
        insert_inner(h, &ctx, pkey, sibling_id, alloc);
        alloc->pwrite(sibling_id, sibling.bytes);
    }

    alloc->pwrite(n_id, n.bytes);
    return {};
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::remove(header* h, const Key& key,
                                              Alloc<PAGE_SIZE>* alloc)
    -> std::optional<Key>
{
    Comp comp;
    auto ctx = find_leaf(*h, key, comp, *alloc);
    node n = {};
    auto n_id = ctx.top().first;
    ctx.pop();
    alloc->pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    if(i == n.leaf.size || n.leaf.keys[i] != key)
    {
        return {};
    }

    Key old = n.leaf.remove(i);
    --h->size;

    if(n_id == h->root)
    {
        alloc->pwrite(n_id, n.bytes);
        return old;
    }

    node p = {};
    auto [p_id, p_i] = ctx.top();
    ctx.pop();
    alloc->pread(p_id, p.bytes);

    if(i == 0 && p_i > 0)
    {
        p.inner.keys[p_i - 1] = n.leaf.keys[0];
        alloc->pwrite(p_id, p.bytes);
    }

    if(n.leaf.size < leaf_node::N_KEYS / 2)
    {
        node left = {};
        uint64_t left_id = 0;
        if(p_i > 0)
        {
            left_id = p.inner.pointers[p_i - 1];
            alloc->pread(left_id, left.bytes);
        }

        node right = {};
        uint64_t right_id = 0;
        if(p_i < p.inner.size)
        {
            right_id = p.inner.pointers[p_i + 1];
            alloc->pread(right_id, right.bytes);
        }

        if(left_id > 0 && left.leaf.size > leaf_node::N_KEYS / 2)
        {
            left.leaf.share_left(&n.leaf, &p.inner.keys[p_i - 1]);
            alloc->pwrite(p_id, p.bytes);
            alloc->pwrite(left_id, left.bytes);
            alloc->pwrite(n_id, n.bytes);
        }
        else if(right_id > 0 && right.leaf.size > leaf_node::N_KEYS / 2)
        {
            n.leaf.share_right(&right.leaf, &p.inner.keys[p_i]);
            alloc->pwrite(p_id, p.bytes);
            alloc->pwrite(n_id, n.bytes);
            alloc->pwrite(right_id, right.bytes);
        }
        else if(left_id > 0)
        {
            left.leaf.merge(&n.leaf);
            remove_inner(h, &ctx, std::move(p), p_id, p_i - 1, alloc);
            alloc->pwrite(left_id, left.bytes);
            alloc->pdelete(n_id);
        }
        else if(right_id > 0)
        {
            n.leaf.merge(&right.leaf);
            remove_inner(h, &ctx, std::move(p), p_id, p_i, alloc);
            alloc->pwrite(n_id, n.bytes);
            alloc->pdelete(right_id);
        }
    }
    else
    {
        alloc->pwrite(n_id, n.bytes);
    }

    return old;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::find(const header& h, const Key& key,
                                            const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    auto it = lower_bound(h, key, Comp(), alloc);
    return (it != end(h, alloc) && *it == key) ? it : end(h, alloc);
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::begin(const header& h,
                                             const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(h.leaf, 0, alloc);
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::end(const header& h,
                                           const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(0, 0, alloc);
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
template<typename CompareFunc>
auto set<Key, Alloc, Comp, PAGE_SIZE>::lower_bound(
    const header& h, const Key& key, CompareFunc comp,
    const Alloc<PAGE_SIZE>& alloc) -> const_iterator
{
    auto ctx = find_leaf(h, key, comp, alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    return i < n.leaf.size ? const_iterator(n_id, i, alloc)
                           : const_iterator(n.leaf.next, 0, alloc);
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
template<typename CompareFunc>
auto set<Key, Alloc, Comp, PAGE_SIZE>::upper_bound(
    const header& h, const Key& key, CompareFunc comp,
    const Alloc<PAGE_SIZE>& alloc) -> const_iterator
{
    auto ctx = find_leaf(h, key, comp, alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);
    uint32_t i = detail::ub(n.leaf, key, comp);

    return i < n.leaf.size ? const_iterator(n_id, i, alloc)
                           : const_iterator(n.leaf.next, 0, alloc);
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
template<typename CompareFunc>
auto set<Key, Alloc, Comp, PAGE_SIZE>::find_leaf(const header& h,
                                                 const Key& key,
                                                 CompareFunc comp,
                                                 const Alloc<PAGE_SIZE>& alloc)
    -> context
{
    context ctx;
    node n = {};
    uint64_t n_id = h.root;
    alloc.pread(n_id, n.bytes);

    while(n.type == node_type::inner)
    {
        uint32_t i = detail::ub(n.inner, key, comp);
        ctx.push({n_id, i});

        n_id = n.inner.pointers[i];
        alloc.pread(n_id, n.bytes);
    }

    ctx.push({n_id, 0});
    return ctx;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::insert_inner(header* h, context* ctx,
                                                    const Key& key,
                                                    uint64_t pointer,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    auto pkey = key;
    while(!ctx->empty())
    {
        node n = {};
        auto [n_id, n_i] = ctx->top();
        ctx->pop();
        alloc->pread(n_id, n.bytes);
        n.inner.insert(n_i, pkey, pointer);

        if(n.inner.size < inner_node::N_KEYS)
        {
            alloc->pwrite(n_id, n.bytes);
            return;
        }

        node sibling = {};
        sibling.inner = inner_node {};
        sibling.type = node_type::inner;

        pointer = alloc->pnew();
        pkey = n.inner.split(&sibling.inner);
        alloc->pwrite(n_id, n.bytes);

        alloc->pwrite(pointer, sibling.bytes);
    }

    node root = {};
    root.inner = inner_node {};
    root.type = node_type::inner;

    uint64_t root_id = alloc->pnew();
    root.inner.pointers[0] = h->root;
    root.inner.insert(0, pkey, pointer);
    alloc->pwrite(root_id, root.bytes);
    h->root = root_id;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::remove_inner(header* h, context* ctx,
                                                    node&& n, uint64_t n_id,
                                                    uint32_t n_i,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    n.inner.remove(n_i);

    while(!ctx->empty() && n.inner.size < inner_node::N_KEYS / 2)
    {
        node p = {};
        auto [p_id, p_i] = ctx->top();
        ctx->pop();
        alloc->pread(p_id, p.bytes);

        node left = {};
        uint64_t left_id = 0;
        if(p_i > 0)
        {
            left_id = p.inner.pointers[p_i - 1];
            alloc->pread(left_id, left.bytes);
        }

        node right = {};
        uint64_t right_id = 0;
        if(p_i < p.inner.size)
        {
            right_id = p.inner.pointers[p_i + 1];
            alloc->pread(right_id, right.bytes);
        }

        if(left_id > 0 && left.inner.size > inner_node::N_KEYS / 2)
        {
            left.inner.share_left(&n.inner, &p.inner.keys[p_i - 1]);
            alloc->pwrite(left_id, left.bytes);
            alloc->pwrite(n_id, n.bytes);
        }
        else if(right_id > 0 && right.inner.size > inner_node::N_KEYS / 2)
        {
            n.inner.share_right(&right.inner, &p.inner.keys[p_i]);
            alloc->pwrite(n_id, n.bytes);
            alloc->pwrite(right_id, right.bytes);
        }
        else if(left_id > 0)
        {
            left.inner.keys[left.inner.size++] = p.inner.keys[p_i - 1];
            left.inner.merge(&n.inner);
            p.inner.remove(p_i - 1);
            alloc->pwrite(left_id, left.bytes);
            alloc->pdelete(n_id);
        }
        else if(right_id > 0)
        {
            n.inner.keys[n.inner.size++] = p.inner.keys[p_i];
            n.inner.merge(&right.inner);
            p.inner.remove(p_i);
            alloc->pwrite(n_id, n.bytes);
            alloc->pdelete(right_id);
        }

        n = std::move(p);
        n_id = p_id;
    }

    if(n_id == h->root && n.inner.size == 0 && n.inner.pointers[0])
    {
        h->root = n.inner.pointers[0];
        alloc->pdelete(n_id);
    }
    else
    {
        alloc->pwrite(n_id, n.bytes);
    }
}

} // namespace disk
