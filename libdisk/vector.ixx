#include <algorithm>
#include <limits>
#include <cmath>
#include <cstdint>

namespace disk
{
auto n_blocks(uint64_t n, uint32_t elements_per_block) -> uint64_t
{
    return std::ceil(n / static_cast<double>(elements_per_block));
}

auto nexti_pow2(uint64_t n) -> uint64_t
{
    return std::pow(2UL, std::ceil(std::log2(n)));
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::create(uint64_t n, Alloc<PAGE_SIZE>* alloc)
    -> header
{
    uint64_t needed = nexti_pow2(n_blocks(n, N_ELEMENTS));
    needed = std::max(needed, 1UL);

    header h;
    h.head = alloc->pnew(needed);
    h.size = n;
    h.capacity = needed * N_ELEMENTS;

    return h;
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::destroy(header* h, Alloc<PAGE_SIZE>* alloc)
{
    alloc->pdelete(h->head, n_blocks(h->capacity, N_ELEMENTS));
    h->head = 0;
    h->size = 0;
    h->capacity = 0;
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::clear(header* h, Alloc<PAGE_SIZE>* alloc)
{
    uint64_t blocks = n_blocks(h->capacity, N_ELEMENTS);
    if(blocks > 1)
    {
        alloc->pdelete(h->head + 1, blocks - 1);
    }

    h->size = 0;
    h->capacity = N_ELEMENTS;
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::read(const header& h, uint64_t i,
                                       const Alloc<PAGE_SIZE>& alloc) -> T
{
    block tmp = {};
    alloc.pread(h.head + i / N_ELEMENTS, tmp.bytes);
    return tmp.elements[i % N_ELEMENTS];
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::read_n(const header& h, uint64_t i,
                                         uint64_t n, T* out,
                                         const Alloc<PAGE_SIZE>& alloc)
{
    block tmp = {};
    uint64_t passed = 0;
    while(passed < n)
    {
        uint64_t block_id = h.head + (i + passed) / N_ELEMENTS;
        uint64_t elements_i = (i + passed) % N_ELEMENTS;
        uint64_t to_pass =
            std::min(n - passed,
                     static_cast<uint64_t>(tmp.elements.size() - elements_i));

        alloc.pread(block_id, tmp.bytes);
        std::copy_n(&tmp.elements[elements_i], to_pass, out + passed);
        passed += to_pass;
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename Iter>
void vector<T, Alloc, PAGE_SIZE>::read_is(const header& h, Iter it, Iter end,
                                          T* out, bool use_i_on_out,
                                          const Alloc<PAGE_SIZE>& alloc,
                                          uint64_t initial_pos)
{
    block tmp = {};
    uint64_t prev_id = std::numeric_limits<uint64_t>::max();

    for(uint64_t passed = 0; it != end; ++passed, ++it)
    {
        uint64_t cur_pos = initial_pos + *it;
        uint32_t cur_id = h.head + cur_pos / N_ELEMENTS;

        if(cur_id != prev_id)
        {
            alloc.pread(cur_id, tmp.bytes);
            prev_id = cur_id;
        }

        out[use_i_on_out ? *it : passed] = tmp.elements[cur_pos % N_ELEMENTS];
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename Func>
void vector<T, Alloc, PAGE_SIZE>::read_lazy(const header& h, uint64_t i,
                                            Func next_is,
                                            const Alloc<PAGE_SIZE>& alloc)
{
    block tmp = {};
    uint64_t prev_id = std::numeric_limits<uint64_t>::max();
    std::optional<uint64_t> opt = i;

    while(opt)
    {
        uint32_t cur_id = h.head + *opt / N_ELEMENTS;

        if(cur_id != prev_id)
        {
            alloc.pread(cur_id, tmp.bytes);
            prev_id = cur_id;
        }

        opt = next_is(tmp.elements[*opt % N_ELEMENTS]);
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::write(const header& h, uint64_t i,
                                        const T& elem, Alloc<PAGE_SIZE>* alloc)
{
    block tmp = {};
    alloc->pread(h.head + i / N_ELEMENTS, tmp.bytes);
    tmp.elements[i % N_ELEMENTS] = elem;
    alloc->pwrite(h.head + i / N_ELEMENTS, tmp.bytes);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::write_n(const header& h, uint64_t i,
                                          uint64_t n, const T* in,
                                          Alloc<PAGE_SIZE>* alloc)
{
    block tmp = {};
    uint64_t passed = 0;
    while(passed < n)
    {
        uint64_t block_id = h.head + (i + passed) / N_ELEMENTS;
        uint64_t elements_i = (i + passed) % N_ELEMENTS;
        uint64_t to_pass =
            std::min(n - passed,
                     static_cast<uint64_t>(tmp.elements.size()) - elements_i);

        if(elements_i > 0 || to_pass < tmp.elements.size())
        {
            alloc->pread(block_id, tmp.bytes);
        }

        std::copy_n(in + passed, to_pass, &tmp.elements[elements_i]);
        alloc->pwrite(block_id, tmp.bytes);
        passed += to_pass;
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename Iter>
void vector<T, Alloc, PAGE_SIZE>::write_is(const header& h, Iter it, Iter end,
                                           T* in, bool use_i_on_in,
                                           Alloc<PAGE_SIZE>* alloc,
                                           uint64_t initial_pos)
{
    block tmp = {};
    uint64_t nil = std::numeric_limits<uint64_t>::max();
    uint64_t prev_id = nil;

    for(uint64_t passed = 0; it != end; ++passed, ++it)
    {
        uint32_t cur_pos = initial_pos + *it;
        uint64_t cur_id = h.head + cur_pos / N_ELEMENTS;

        if(cur_id != prev_id)
        {
            if(prev_id != nil)
            {
                alloc->pwrite(prev_id, tmp.bytes);
            }

            alloc->pread(cur_id, tmp.bytes);
            prev_id = cur_id;
        }

        tmp.elements[cur_pos % N_ELEMENTS] = in[use_i_on_in ? *it : passed];
    }

    alloc->pwrite(prev_id, tmp.bytes);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename Func>
void vector<T, Alloc, PAGE_SIZE>::write_lazy(const header& h, uint64_t i,
                                             Func next_is,
                                             Alloc<PAGE_SIZE>* alloc)
{
    block tmp = {};
    uint64_t nil = std::numeric_limits<uint64_t>::max();
    uint64_t prev_id = nil;
    std::optional<uint64_t> opt = i;

    while(opt)
    {
        uint64_t cur_id = h.head + *opt / N_ELEMENTS;

        if(cur_id != prev_id)
        {
            if(prev_id != nil)
            {
                alloc->pwrite(prev_id, tmp.bytes);
            }

            alloc->pread(cur_id, tmp.bytes);
            prev_id = cur_id;
        }

        opt = next_is(&tmp.elements[*opt % N_ELEMENTS]);
    }

    alloc->pwrite(prev_id, tmp.bytes);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::fill(const header& h, const T& elem,
                                       Alloc<PAGE_SIZE>* alloc)
{
    if(uint64_t n = n_blocks(h.size, N_ELEMENTS); n > 0)
    {
        block tmp = {};
        std::fill(tmp.elements.begin(), tmp.elements.end(), elem);

        for(uint64_t i = 0; i < n; ++i)
        {
            alloc->pwrite(h.head + i, tmp.bytes);
        }
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename Function>
void vector<T, Alloc, PAGE_SIZE>::transform_n(const header& h, uint64_t i,
                                              uint64_t n, Function f,
                                              Alloc<PAGE_SIZE>* alloc)
{
    block tmp = {};
    uint64_t passed = 0;
    while(passed < n)
    {
        uint64_t block_id = h.head + (i + passed) / N_ELEMENTS;
        uint64_t start = (i + passed) % N_ELEMENTS;
        uint64_t to_pass = std::min(
            n - passed, static_cast<uint64_t>(tmp.elements.size()) - start);

        alloc->pread(block_id, tmp.bytes);

        for(uint64_t i = 0; i < to_pass; ++i)
        {
            tmp.elements[start + i] = f(tmp.elements[start + i]);
        }

        alloc->pwrite(block_id, tmp.bytes);
        passed += to_pass;
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::push_back(header* h, const T& elem,
                                            Alloc<PAGE_SIZE>* alloc)
{
    uint64_t n = h->size;
    resize(h, n + 1, alloc);
    write(*h, n, elem, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::pop_back(header* h, Alloc<PAGE_SIZE>* alloc)
{
    resize(h, h->size - 1, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::back(const header& h,
                                       const Alloc<PAGE_SIZE>& alloc) -> T
{
    return read(h, h.size - 1, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::resize(header* h, uint64_t n,
                                         Alloc<PAGE_SIZE>* alloc)
{
    uint64_t available = n_blocks(h->capacity, N_ELEMENTS);
    uint64_t needed = nexti_pow2(n_blocks(n, N_ELEMENTS));

    if(needed > available)
    {
        uint32_t new_available = std::pow(2, (uint32_t)std::log2(needed) + 1);
        alloc->prenew(&h->head, available, new_available);
        h->capacity = new_available * N_ELEMENTS;
    }

    h->size = n;
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::reserve(header* h, uint64_t n,
                                          Alloc<PAGE_SIZE>* alloc)
{
    uint64_t available = n_blocks(h->capacity, N_ELEMENTS);
    uint64_t needed = nexti_pow2(n_blocks(n, N_ELEMENTS));

    if(needed > available)
    {
        uint32_t new_available = std::pow(2, (uint32_t)std::log2(needed) + 1);
        alloc->prenew(&h->head, available, new_available);
        h->capacity = new_available * N_ELEMENTS;
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<T, Alloc, PAGE_SIZE>::shrink_to_fit(header* h,
                                                Alloc<PAGE_SIZE>* alloc)
{
    uint64_t available = n_blocks(h->capacity, N_ELEMENTS);
    uint64_t needed = n_blocks(h->size, N_ELEMENTS);

    if(available > needed)
    {
        h->capacity = needed * N_ELEMENTS;
        alloc->pdelete(h->head + needed, available - needed);
    }
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::begin(const header& h,
                                        Alloc<PAGE_SIZE>* alloc) -> iterator
{
    return iterator(h.head, 0, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::iterator_at(const header& h, uint64_t i,
                                              Alloc<PAGE_SIZE>* alloc)
    -> iterator
{
    return iterator(h.head, i, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::end(const header& h, Alloc<PAGE_SIZE>* alloc)
    -> iterator
{
    return iterator(h.head, h.size, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::rbegin(const header& h,
                                         Alloc<PAGE_SIZE>* alloc)
    -> reverse_iterator
{
    return reverse_iterator(h.head, h.size, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::riterator_at(const header& h, uint64_t i,
                                               Alloc<PAGE_SIZE>* alloc)
    -> reverse_iterator
{
    return reverse_iterator(h.head, i + 1, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::rend(const header& h, Alloc<PAGE_SIZE>* alloc)
    -> reverse_iterator
{
    return reverse_iterator(h.head, 0, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::begin(const header& h,
                                        const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(h.head, 0, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::iterator_at(const header& h, uint64_t i,
                                              const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(h.head, i, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::end(const header& h,
                                      const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(h.head, h.size, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::rbegin(const header& h,
                                         const Alloc<PAGE_SIZE>& alloc)
    -> const_reverse_iterator
{
    return const_reverse_iterator(h.head, h.size, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::riterator_at(const header& h, uint64_t i,
                                               const Alloc<PAGE_SIZE>& alloc)
    -> const_reverse_iterator
{
    return const_reverse_iterator(h.head, i + 1, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<T, Alloc, PAGE_SIZE>::rend(const header& h,
                                       const Alloc<PAGE_SIZE>& alloc)
    -> const_reverse_iterator
{
    return const_reverse_iterator(h.head, 0, alloc);
}

} // namespace disk
