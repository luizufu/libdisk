#include <libdisk/detail/utility.hxx>

#include <cmath>

namespace disk
{
template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::leaf_node::insert(
    uint32_t i, const Key& key, const Value& value)
{
    std::move_backward(detail::kbegin(this) + i, detail::kend(this),
                       detail::kend(this) + 1);
    std::move_backward(detail::vbegin(this) + i, detail::vend(this),
                       detail::vend(this) + 1);
    keys[i] = key;
    values[i] = value;
    ++size;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::inner_node::insert(
    uint32_t i, const Key& key, uint64_t pointer)
{
    std::move_backward(detail::kbegin(this) + i, detail::kend(this),
                       detail::kend(this) + 1);
    std::move_backward(detail::pbegin(this) + i + 1, detail::pend(this),
                       detail::pend(this) + 1);
    keys[i] = key;
    pointers[i + 1] = pointer;
    ++size;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::leaf_node::remove(uint32_t i)
    -> Value
{
    Value value = values[i];
    std::move(detail::kbegin(this) + i + 1, detail::kend(this),
              detail::kbegin(this) + i);
    std::move(detail::vbegin(this) + i + 1, detail::vend(this),
              detail::vbegin(this) + i);
    --size;
    return value;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::inner_node::remove(uint32_t i)
    -> uint64_t
{
    auto pointer = std::move(pointers[i + 1]);
    std::move(detail::kbegin(this) + i + 1, detail::kend(this),
              detail::kbegin(this) + i);
    std::move(detail::pbegin(this) + i + 2, detail::pend(this),
              detail::pbegin(this) + i + 1);
    --size;
    return pointer;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::leaf_node::split(
    leaf_node* s, uint64_t pointer1, uint64_t pointer2) -> Key
{
    uint32_t mid = size / 2;
    std::move(detail::kbegin(this) + mid, detail::kend(this),
              detail::kbegin(s));
    std::move(detail::vbegin(this) + mid, detail::vend(this),
              detail::vbegin(s));
    s->size = size - mid;
    size = mid;
    s->next = next;
    s->prev = pointer1;
    next = pointer2;

    return s->keys[0];
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::inner_node::split(inner_node* s)
    -> Key
{
    uint32_t mid = this->size / 2;

    std::move(detail::kbegin(this) + mid + 1, detail::kend(this),
              detail::kbegin(s));
    std::move(detail::pbegin(this) + mid + 1, detail::pend(this),
              detail::pbegin(s));
    s->size = size - mid - 1;
    size = mid;

    return keys[size];
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::leaf_node::share_to(
    leaf_node* s, Key* p_key, uint32_t to_pass, bool no_merge)
{
    bool is_merge = to_pass == size;
    if((to_pass = std::min({to_pass, N_KEYS - s->size - 1, size})) > 0)
    {
        std::move_backward(detail::kbegin(s), detail::kend(s),
                           detail::kend(s) + to_pass);
        std::move_backward(detail::vbegin(s), detail::vend(s),
                           detail::vend(s) + to_pass);
        std::move(detail::kend(this) - to_pass, detail::kend(this),
                  detail::kbegin(s));
        std::move(detail::vend(this) - to_pass, detail::vend(this),
                  detail::vbegin(s));
        size -= to_pass;
        s->size += to_pass;
        if(!no_merge && is_merge)
        {
            next = 0;
            s->prev = prev;
            prev = 0;
        }
        *p_key = s->keys[0];
    }
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::inner_node::share_to(
    inner_node* s, Key* p_key, uint32_t to_pass, bool no_merge)
{
    if((to_pass = std::min({to_pass, N_KEYS - s->size - 1, size})) > 0)
    {
        bool is_merge =
            !no_merge && size == to_pass && s->size + to_pass < N_KEYS - 1;
        keys[size] = *p_key;
        *p_key = keys[size - to_pass];

        std::move_backward(detail::kbegin(s), detail::kend(s),
                           detail::kend(s) + to_pass + is_merge);
        std::move_backward(detail::pbegin(s), detail::pend(s),
                           detail::pend(s) + to_pass + is_merge);
        std::move(detail::kend(this) + 1 - to_pass - is_merge,
                  detail::kend(this) + 1, detail::kbegin(s));
        std::move(detail::pend(this) - to_pass - is_merge, detail::pend(this),
                  detail::pbegin(s));

        size -= to_pass;
        s->size += to_pass + is_merge;
    }
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::leaf_node::share_from(
    leaf_node* s, Key* p_key, uint32_t to_pass, bool no_merge)
{
    bool is_merge = to_pass == s->size;
    if((to_pass = std::min({to_pass, N_KEYS - size - 1, s->size})) > 0)
    {
        std::move(detail::kbegin(s), detail::kbegin(s) + to_pass,
                  detail::kend(this));
        std::move(detail::vbegin(s), detail::vbegin(s) + to_pass,
                  detail::vend(this));
        std::move(detail::kbegin(s) + to_pass, detail::kend(s),
                  detail::kbegin(s));
        std::move(detail::vbegin(s) + to_pass, detail::vend(s),
                  detail::vbegin(s));
        size += to_pass;
        s->size -= to_pass;
        if(!no_merge && is_merge)
        {
            next = s->next;
            s->next = 0;
            s->prev = 0;
        }
        *p_key = s->keys[0];
    }
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::inner_node::share_from(
    inner_node* s, Key* p_key, uint32_t to_pass, bool no_merge)
{
    if((to_pass = std::min({to_pass, N_KEYS - size - 1, s->size})) > 0)
    {
        bool is_merge =
            !no_merge && s->size == to_pass && size + to_pass < N_KEYS - 1;
        keys[size] = *p_key;
        *p_key = s->keys[to_pass - 1];

        std::move(detail::kbegin(s), detail::kbegin(s) + to_pass - 1 + is_merge,
                  detail::kend(this) + 1);
        std::move(detail::pbegin(s), detail::pbegin(s) + to_pass + is_merge,
                  detail::pend(this));
        std::move(detail::kbegin(s) + to_pass, detail::kend(s),
                  detail::kbegin(s));
        std::move(detail::pbegin(s) + to_pass + is_merge, detail::pend(s),
                  detail::pbegin(s));

        size += to_pass + is_merge;
        s->size -= to_pass;
    }
}

} // namespace disk
