#pragma once

#include <array>

#include <libdisk/detail/utility.hxx>

namespace disk
{
template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE = 4096>
class array
{
    static const uint64_t N_ELEMENTS = PAGE_SIZE / sizeof(T);
    static const uint64_t N_BLOCKS =
        detail::ceil(N / static_cast<float>(N_ELEMENTS));

public:
    union block
    {
        std::array<T, N_ELEMENTS> elements;
        std::byte bytes[PAGE_SIZE];
    };

    struct header
    {
        uint64_t head = 0;
        uint64_t size = N;
    };

    class const_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using reference = const value_type&;
        using pointer = const value_type*;
        using iterator_category = std::forward_iterator_tag;

        const_iterator(uint64_t head, uint64_t i,
                       const Alloc<PAGE_SIZE>& alloc);

        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_iterator&;
        auto operator++(int) -> const_iterator;
        auto operator==(const const_iterator& other) const -> bool;
        auto operator!=(const const_iterator& other) const -> bool;

    private:
        block _b;
        uint64_t _head;
        uint64_t _i;
        const Alloc<PAGE_SIZE>& _alloc;
    };

    static auto create(Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);

    static auto read(const header& h, uint64_t i, const Alloc<PAGE_SIZE>& alloc)
        -> T;
    static void write(const header& h, uint64_t i, const T& elem,
                      Alloc<PAGE_SIZE>* alloc);
    static void fill(const header& h, const T& elem, Alloc<PAGE_SIZE>* alloc);

    static auto begin(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;
    static auto end(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> const_iterator;
};

} // namespace disk

#include <libdisk/array-iterator.ixx>
#include <libdisk/array.ixx>
