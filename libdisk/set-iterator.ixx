namespace disk
{
template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
set<Key, Alloc, Comp, PAGE_SIZE>::const_iterator::const_iterator(
    uint64_t leaf, uint32_t i, const Alloc<PAGE_SIZE>& alloc)
    : _n {}
    , _id(leaf)
    , _i(i)
    , _alloc(alloc)
{
    if(_id > 0)
    {
        _alloc.pread(_id, _n.bytes);
        if(_n.leaf.size == 0)
        {
            _id = 0;
            _i = 0;
        }
    }
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::const_iterator::operator*() const
    -> reference
{
    return _n.leaf.keys[_i];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::const_iterator::operator->() const
    -> pointer
{
    return &_n.leaf.keys[_i];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::const_iterator::operator++()
    -> const_iterator&
{
    ++_i;

    if(_i >= _n.leaf.size)
    {
        _i = 0;
        _id = _n.leaf.next;
        if(_id > 0)
        {
            _alloc.pread(_id, _n.bytes);
        }
    }

    return *this;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::const_iterator::operator++(int)
    -> const_iterator
{
    const_iterator old_value(*this);
    ++*this;
    return old_value;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::const_iterator::operator==(
    const const_iterator& other) const -> bool
{
    return other._id == _id && other._i == _i;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::const_iterator::operator!=(
    const const_iterator& other) const -> bool
{
    return other._id != _id || other._i != _i;
}

} // namespace disk
