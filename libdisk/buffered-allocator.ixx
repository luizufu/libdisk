#include <libcache/lru.hxx>
#include <libdisk/detail/utility.hxx>

#include <unistd.h>
#include <cstdio>

namespace disk
{
template<size_t PAGE_SIZE>
buffered_allocator<PAGE_SIZE>::buffered_allocator(const char* filename,
                                                  size_t cache_pages)
    : _header {.sequential_accesses = 0,
               .sequential_writes = 0,
               .random_accesses = 0,
               .random_writes = 0,
               .flists = {},
               .pages = 0,
               .last_i = 0}
    , _cache(std::make_unique<cache::lru>(cache_pages, PAGE_SIZE))
    , _has_cache(cache_pages > 0)
{
    _file = fopen(filename, "w+b");

    if(fseek(_file, 0, SEEK_END) == 0)
    {
        ftruncate(fileno(_file), 1 * PAGE_SIZE);
        ++_header.pages;
    }
    else
    {
        fseek(_file, 0, SEEK_SET);
        fread(_header.bytes, sizeof(std::byte), PAGE_SIZE, _file);
    }
}

template<size_t PAGE_SIZE>
buffered_allocator<PAGE_SIZE>::~buffered_allocator() noexcept
{
    for(auto& [size, lazy_free_list]: _lazy_free_map)
    {
        while(!lazy_free_list.empty())
        {
            auto [id, extract] = lazy_free_list.front();
            lazy_free_list.pop();
            for(auto cid: extract(id, this))
            {
                lazy_free_list.push({cid, extract});
            }
            pdelete(id, size);
        }
    }

    fseek(_file, 0, SEEK_SET);
    fwrite(_header.bytes, sizeof(std::byte), PAGE_SIZE, _file);

    fclose(_file);
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::pnew(uint64_t size) -> uint64_t
{
    auto lazy_it = _lazy_free_map.find(size);
    if(lazy_it != _lazy_free_map.end())
    {
        auto [id, extract] = lazy_it->second.front();
        lazy_it->second.pop();

        for(auto cid: extract(id, this))
        {
            lazy_it->second.push({cid, extract});
        }

        if(lazy_it->second.empty())
        {
            _lazy_free_map.erase(lazy_it);
        }

        return id;
    }

    auto free_it = detail::fllb(&_header.flists, size);
    if(free_it != detail::flend(&_header.flists))
    {
        auto [it_size, it_root] = *free_it;

        uint64_t index = it_root;
        page tmp = {};
        pread(it_root, tmp.bytes);
        free_it->second = tmp.next;

        if(!tmp.next)
        {
            detail::flrem(&_header.flists, free_it);
        }

        if(it_size > size)
        {
            pdelete(index + size, it_size - size);
        }

        return index;
    }

    uint64_t index = _header.pages;
    ftruncate(fileno(_file), (_header.pages + size) * PAGE_SIZE);
    _header.pages += size;

    return index;
}

template<size_t PAGE_SIZE>
void buffered_allocator<PAGE_SIZE>::pdelete(uint64_t root, uint64_t size)
{
    if(root + size == _header.pages)
    {
        ftruncate(fileno(_file), (_header.pages - size) * PAGE_SIZE);
        _header.pages -= size;
        return;
    }

    auto it = detail::fllb(&_header.flists, size);
    if(it->first != size)
    {
        detail::flins(&_header.flists, it, {size, 0});
    }

    page tmp = {.next = it->second};
    it->second = root;
    pwrite(root, tmp.bytes);
}

template<size_t PAGE_SIZE>
void buffered_allocator<PAGE_SIZE>::pdelete_lazy(uint64_t root, extract_func f,
                                                 uint64_t size)
{
    _lazy_free_map[size].push({root, f});
}

template<size_t PAGE_SIZE>
void buffered_allocator<PAGE_SIZE>::prenew(uint64_t* root, uint64_t size_before,
                                           uint64_t size_after)
{
    if(*root + size_before == _header.pages)
    {
        ftruncate(fileno(_file),
                  (_header.pages + size_after - size_before) * PAGE_SIZE);
        _header.pages += size_after - size_before;
        return;
    }

    auto it = detail::fllb(&_header.flists, size_before);
    if(it->first != size_before)
    {
        detail::flins(&_header.flists, it, {size_before, 0});
    }

    page tmp_old = {.next = it->second};
    it->second = *root;

    uint64_t new_root = pnew(size_after);

    page tmp = {};
    for(size_t i = 1; i < size_before; ++i)
    {
        pread(*root + i, tmp.bytes);
        pwrite(new_root + i, tmp.bytes);
    }

    pwrite(*root, tmp_old.bytes);
    *root = new_root;
}

template<size_t PAGE_SIZE>
void buffered_allocator<PAGE_SIZE>::pread(uint64_t i, std::byte* page) const
{
    if(i >= _header.pages)
    {
        throw std::out_of_range("No page at this index");
    }

    if(!_has_cache || !_cache->try_read(i, page))
    {
        fseek(_file, i * PAGE_SIZE, SEEK_SET);
        fread(page, sizeof(std::byte), PAGE_SIZE, _file);
        if(_has_cache)
        {
            _cache->write(i, page);
        }

        if(_header.last_i == i || _header.last_i + 1 == i
           || (_header.last_i > 0 && _header.last_i - 1 == i))
        {
            ++_header.sequential_accesses;
        }
        else
        {
            ++_header.random_accesses;
        }

        _header.last_i = i;
    }
}

template<size_t PAGE_SIZE>
void buffered_allocator<PAGE_SIZE>::pwrite(uint64_t i, const std::byte* page)
{
    if(i >= _header.pages)
    {
        throw std::out_of_range("No page at this index");
    }

    fseek(_file, i * PAGE_SIZE, SEEK_SET);
    fwrite(page, sizeof(std::byte), PAGE_SIZE, _file);
    if(_has_cache)
    {
        _cache->write(i, page);
    }

    if(_header.last_i == i || _header.last_i + 1 == i
       || (_header.last_i > 0 && _header.last_i - 1 == i))
    {
        ++_header.sequential_writes;
    }
    else
    {
        ++_header.random_writes;
    }

    _header.last_i = i;
}

template<size_t PAGE_SIZE>
void buffered_allocator<PAGE_SIZE>::clear()
{
    _header.pages = 0;
    detail::flclr(&_header.flists);
    _lazy_free_map.clear();
    if(_has_cache)
    {
        _cache->clear();
    }
    reset_benchmark();

    ftruncate(fileno(_file), static_cast<off_t>(0));
    pnew(1);
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::pages() const -> uint64_t
{
    return _header.pages;
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::size() const -> uint64_t
{
    return pages() * PAGE_SIZE;
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::empty() const -> bool
{
    return _header.pages == 1;
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::cache() const -> const cache::lru&
{
    return *_cache;
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::sequential_accesses() const -> uint64_t
{
    return _header.sequential_accesses;
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::sequential_writes() const -> uint64_t
{
    return _header.sequential_writes;
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::random_accesses() const -> uint64_t
{
    return _header.random_accesses;
}

template<size_t PAGE_SIZE>
auto buffered_allocator<PAGE_SIZE>::random_writes() const -> uint64_t
{
    return _header.random_writes;
}

template<size_t PAGE_SIZE>
void buffered_allocator<PAGE_SIZE>::reset_benchmark() const
{
    _header.sequential_accesses = 0;
    _header.sequential_writes = 0;
    _header.random_accesses = 0;
    _header.random_writes = 0;
}

} // namespace disk
