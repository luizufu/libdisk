namespace disk
{

// iterator

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
vector<Key, Alloc, PAGE_SIZE>::iterator::iterator(uint64_t head, uint64_t i,
                                                  Alloc<PAGE_SIZE>* alloc)
    : _b {}
    , _head(head)
    , _i(i)
    , _alloc(alloc)
{
    _alloc->pread(_head + _i / _b.elements.size(), _b.bytes);
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::iterator::operator*() -> reference
{
    return _b.elements[_i % _b.elements.size()];
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::iterator::operator->() -> pointer
{
    return &_b.elements[_i % _b.elements.size()];
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::iterator::operator++() -> iterator&
{
    ++_i;

    if(_i % _b.elements.size() == 0)
    {
        _alloc->pwrite(_head + (_i - 1) / _b.elements.size(), _b.bytes);
        _alloc->pread(_head + _i / _b.elements.size(), _b.bytes);
    }

    return *this;
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::iterator::operator++(int) -> iterator
{
    iterator old_value(*this);
    ++*this;
    return old_value;
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::iterator::operator==(
    const iterator& other) const -> bool
{
    uint64_t n = _b.elements.size();
    return _head + _i / n == other._head + other._i / n
           && _i % n == other._i % n;
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::iterator::operator!=(
    const iterator& other) const -> bool
{
    uint64_t n = _b.elements.size();
    return _head + _i / n != other._head + other._i / n
           || _i % n != other._i % n;
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
void vector<Key, Alloc, PAGE_SIZE>::iterator::flush()
{
    if(_i % _b.elements.size() != 0)
    {
        _alloc->pwrite(_head + _i / _b.elements.size(), _b.bytes);
    }
}

// const_iterator
template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
vector<Key, Alloc, PAGE_SIZE>::const_iterator::const_iterator(
    uint64_t head, uint64_t i, const Alloc<PAGE_SIZE>& alloc)
    : _b {}
    , _head(head)
    , _i(i)
    , _alloc(alloc)
{
    _alloc.pread(_head + _i / _b.elements.size(), _b.bytes);
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::const_iterator::operator*() const
    -> reference
{
    return _b.elements[_i % _b.elements.size()];
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::const_iterator::operator->() const
    -> pointer
{
    return &_b.elements[_i % _b.elements.size()];
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::const_iterator::operator++()
    -> const_iterator&
{
    ++_i;

    if(_i % _b.elements.size() == 0)
    {
        _alloc.pread(_head + _i / _b.elements.size(), _b.bytes);
    }

    return *this;
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::const_iterator::operator++(int)
    -> const_iterator
{
    const_iterator old_value(*this);
    ++*this;
    return old_value;
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::const_iterator::operator==(
    const const_iterator& other) const -> bool
{
    uint64_t n = _b.elements.size();
    return _head + _i / n == other._head + other._i / n
           && _i % n == other._i % n;
}

template<typename Key, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto vector<Key, Alloc, PAGE_SIZE>::const_iterator::operator!=(
    const const_iterator& other) const -> bool
{
    uint64_t n = _b.elements.size();
    return _head + _i / n != other._head + other._i / n
           || _i % n != other._i % n;
}

} // namespace disk
