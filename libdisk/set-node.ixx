#include <libdisk/detail/utility.hxx>

#include <cmath>

namespace disk
{
template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::leaf_node::insert(uint32_t i,
                                                         const Key& key)
{
    std::move_backward(detail::kbegin(this) + i, detail::kend(this),
                       detail::kend(this) + 1);
    keys[i] = key;
    ++size;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::inner_node::insert(uint32_t i,
                                                          const Key& key,
                                                          uint64_t pointer)
{
    std::move_backward(detail::kbegin(this) + i, detail::kend(this),
                       detail::kend(this) + 1);
    std::move_backward(detail::pbegin(this) + i + 1, detail::pend(this),
                       detail::pend(this) + 1);
    keys[i] = key;
    pointers[i + 1] = pointer;
    ++size;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::leaf_node::remove(uint32_t i) -> Key
{
    Key key = keys[i];
    std::move(detail::kbegin(this) + i + 1, detail::kend(this),
              detail::kbegin(this) + i);
    --size;

    return key;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::inner_node::remove(uint32_t i)
    -> uint64_t
{
    uint64_t pointer = pointers[i + 1];
    std::move(detail::kbegin(this) + i + 1, detail::kend(this),
              detail::kbegin(this) + i);
    std::move(detail::pbegin(this) + i + 2, detail::pend(this),
              detail::pbegin(this) + i + 1);
    --size;

    return pointer;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::leaf_node::split(leaf_node* s,
                                                        uint64_t as) -> Key
{
    uint32_t mid = size / 2;

    std::move(detail::kbegin(this) + mid, detail::kend(this),
              detail::kbegin(s));
    s->size = size - mid;
    size = mid;
    s->next = next;
    next = as;

    return s->keys[0];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
auto set<Key, Alloc, Comp, PAGE_SIZE>::inner_node::split(inner_node* s) -> Key
{
    uint32_t mid = this->size / 2;

    std::move(detail::kbegin(this) + mid + 1, detail::kend(this),
              detail::kbegin(s));
    std::move(detail::pbegin(this) + mid + 1, detail::pend(this),
              detail::pbegin(s));
    s->size = size - mid - 1;
    size = mid;

    return keys[size];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::leaf_node::share_left(leaf_node* s,
                                                             Key* p_key)
{
    uint32_t half = std::ceil((size + s->size) / 2.F);
    uint32_t to_pass = size - half;

    std::move_backward(detail::kbegin(s), detail::kend(s),
                       detail::kend(s) + to_pass);
    std::move(detail::kbegin(this) + half, detail::kend(this),
              detail::kbegin(s));

    size -= to_pass;
    s->size += to_pass;

    *p_key = s->keys[0];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::inner_node::share_left(inner_node* s,
                                                              Key* p_key)
{
    uint32_t half = std::ceil((size + s->size) / 2.F);
    uint32_t to_pass = size - half;

    std::move_backward(detail::kbegin(s), detail::kend(s),
                       detail::kend(s) + to_pass);
    std::move(detail::kbegin(this) + half + 1, detail::kend(this),
              detail::kbegin(s));
    s->keys[to_pass - 1] = *p_key;

    std::move_backward(detail::pbegin(s), detail::pend(s),
                       detail::pend(s) + to_pass);
    std::move(detail::pbegin(this) + half + 1, detail::pend(this),
              detail::pbegin(s));

    size -= to_pass;
    s->size += to_pass;

    *p_key = keys[size];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::leaf_node::share_right(leaf_node* s,
                                                              Key* p_key)
{
    uint32_t half = std::ceil((s->size + size) / 2.F);
    uint32_t to_pass = s->size - half;

    std::move(detail::kbegin(s), detail::kbegin(s) + to_pass,
              detail::kend(this));
    std::move(detail::kbegin(s) + to_pass, detail::kend(s), detail::kbegin(s));

    size += to_pass;
    s->size -= to_pass;

    *p_key = s->keys[0];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::inner_node::share_right(inner_node* s,
                                                               Key* p_key)
{
    uint32_t half = std::ceil((s->size + size) / 2.F);
    uint32_t to_pass = s->size - half;

    keys[size++] = *p_key;

    std::move(detail::kbegin(s), detail::kbegin(s) + to_pass,
              detail::kend(this));
    std::move(detail::kbegin(s) + to_pass, detail::kend(s), detail::kbegin(s));

    std::move(detail::pbegin(s), detail::pbegin(s) + to_pass,
              detail::pend(this) - 1);
    std::move(detail::pbegin(s) + to_pass, detail::pend(s), detail::pbegin(s));

    size += to_pass - 1;
    s->size -= to_pass;

    *p_key = keys[size];
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::leaf_node::merge(leaf_node* s)
{
    std::move(detail::kbegin(s), detail::kend(s), detail::kbegin(this) + size);
    next = s->next;
    size += s->size;
    s->next = 0;
    s->size = 0;
}

template<typename Key, template<size_t> typename Alloc, typename Comp,
         size_t PAGE_SIZE>
void set<Key, Alloc, Comp, PAGE_SIZE>::inner_node::merge(inner_node* s)
{
    std::move(detail::kbegin(s), detail::kend(s), detail::kbegin(this) + size);
    std::move(detail::pbegin(s), detail::pend(s), detail::pbegin(this) + size);
    size += s->size;
    s->size = 0;
}

} // namespace disk
