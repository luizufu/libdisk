#include <vector>
#include <algorithm>
#include <iostream>

namespace disk
{
template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto array<T, N, Alloc, PAGE_SIZE>::create(Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = {};
    h.head = alloc->pnew(N_BLOCKS);
    h.size = N;

    return h;
}

template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void array<T, N, Alloc, PAGE_SIZE>::destroy(header* h, Alloc<PAGE_SIZE>* alloc)
{
    alloc->pdelete(h->head, N_BLOCKS);
    h->head = 0;
    h->size = 0;
}

template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto array<T, N, Alloc, PAGE_SIZE>::read(const header& h, uint64_t i,
                                         const Alloc<PAGE_SIZE>& alloc) -> T
{
    block tmp = {};
    alloc.pread(h.head + i / N_ELEMENTS, tmp.bytes);
    return tmp.elements[i % N_ELEMENTS];
}

template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void array<T, N, Alloc, PAGE_SIZE>::write(const header& h, uint64_t i,
                                          const T& elem,
                                          Alloc<PAGE_SIZE>* alloc)
{
    block tmp = {};
    alloc->pread(h.head + i / N_ELEMENTS, tmp.bytes);
    tmp.elements[i % N_ELEMENTS] = elem;
    alloc->pwrite(h.head + i / N_ELEMENTS, tmp.bytes);
}

template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void array<T, N, Alloc, PAGE_SIZE>::fill(const header& h, const T& elem,
                                         Alloc<PAGE_SIZE>* alloc)
{
    block tmp = {};
    std::fill_n(tmp.elements, N_ELEMENTS, elem);

    for(uint64_t i = 0; i < N_BLOCKS; ++i)
    {
        alloc->pwrite(h.head + i, tmp.bytes);
    }
}

template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto array<T, N, Alloc, PAGE_SIZE>::begin(const header& h,
                                          const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(h.head, 0, alloc);
}

template<typename T, uint64_t N, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto array<T, N, Alloc, PAGE_SIZE>::end(const header& h,
                                        const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(h.head, h.size, alloc);
}

} // namespace disk
