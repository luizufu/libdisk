#include <libdisk/detail/utility.hxx>

#include <algorithm>

namespace disk
{
template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::create(Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    h.root = alloc->pnew();
    h.first_leaf = h.root;
    h.last_leaf = h.root;
    h.size = 0;
    h.height = 0;

    node root = {};
    root.leaf = leaf_node {};
    root.type = node_type::leaf;

    alloc->pwrite(h.root, root.bytes);

    return h;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::destroy(header* h,
                                                      Alloc<PAGE_SIZE>* alloc)
{
    node n = {};
    alloc->pread(h->root, n.bytes);

    if(n.type == node_type::inner)
    {
        header h2 = {};
        for(uint32_t i = 0; i < n.inner.size + 1; ++i)
        {
            h2.root = n.inner.pointers[i];
            destroy(&h2, alloc);
        }
    }

    alloc->pdelete(h->root);
    h->root = 0;
    h->first_leaf = 0;
    h->last_leaf = 0;
    h->size = 0;
    h->height = 0;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::destroy_lazy(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    alloc->pdelete_lazy(h->root,
                        [](uint64_t id, Alloc<PAGE_SIZE>* alloc2)
                        {
                            std::vector<uint64_t> children;
                            node n = {};
                            alloc2->pread(id, n.bytes);
                            if(n.type == node_type::inner)
                            {
                                children.insert(
                                    children.end(), &n.inner.pointers[0],
                                    &n.inner.pointers[0] + n.inner.size + 1);
                            }

                            return children;
                        });
    h->root = 0;
    h->first_leaf = 0;
    h->last_leaf = 0;
    h->size = 0;
    h->height = 0;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::clear(header* h,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    node n = {};
    alloc->pread(h->root, n.bytes);

    if(n.type == node_type::inner)
    {
        header h2 = {};
        for(uint32_t i = 0; i < n.inner.size + 1; ++i)
        {
            h2.root = n.inner.pointers[i];
            destroy(&h2, alloc);
        }
    }

    n.leaf = leaf_node {};
    n.type = node_type::leaf;
    alloc->pwrite(h->root, n.bytes);
    h->first_leaf = h->root;
    h->last_leaf = h->root;
    h->size = 0;
    h->height = 0;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::clear_lazy(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    node n = {};
    alloc->pread(h->root, n.bytes);

    if(n.type == node_type::inner)
    {
        header h2 = {};
        for(uint32_t i = 0; i < n.inner.size + 1; ++i)
        {
            h2.root = n.inner.pointers[i];
            destroy_lazy(&h2, alloc);
        }
    }

    n.leaf = leaf_node {};
    n.type = node_type::leaf;
    alloc->pwrite(h->root, n.bytes);
    h->first_leaf = h->root;
    h->last_leaf = h->root;
    h->size = 0;
    h->height = 0;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::insert(header* h, const Key& key,
                                                     const Value& value,
                                                     Alloc<PAGE_SIZE>* alloc)
    -> bool
{
    Comp comp;
    auto ctx = find_leaf(h->root, key, comp, *alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    ctx.pop();
    alloc->pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    if(i != n.leaf.size && n.leaf.keys[i] == key)
    {
        return false;
    }

    n.leaf.insert(i, key, value);
    ++h->size;

    if(n.leaf.size >= leaf_node::N_KEYS)
    {
        node sibling = {};
        sibling.leaf = leaf_node {};
        sibling.type = node_type::leaf;

        uint64_t sibling_id = alloc->pnew();
        Key pkey = n.leaf.split(&sibling.leaf, n_id, sibling_id);
        if(sibling.leaf.next > 0)
        {
            node next = {};
            alloc->pread(sibling.leaf.next, next.bytes);
            next.leaf.prev = sibling_id;
            alloc->pwrite(sibling.leaf.next, next.bytes);
        }

        if(n_id == h->last_leaf)
        {
            h->last_leaf = sibling_id;
        }
        if(auto new_root = insert_inner(&ctx, pkey, sibling_id, alloc))
        {
            uint64_t new_root_id = alloc->pnew();
            new_root->inner.pointers[0] = h->root;
            alloc->pwrite(new_root_id, new_root->bytes);
            h->root = new_root_id;
            ++h->height;
        }

        alloc->pwrite(sibling_id, sibling.bytes);
    }

    alloc->pwrite(n_id, n.bytes);
    return true;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::insert_or_update(
    header* h, const Key& key, const Value& value, Alloc<PAGE_SIZE>* alloc)
    -> std::optional<Value>
{
    Comp comp;
    auto ctx = find_leaf(h->root, key, comp, *alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    ctx.pop();
    alloc->pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    if(i != n.leaf.size && n.leaf.keys[i] == key)
    {
        Value old = n.leaf.values[i];
        n.leaf.values[i] = value;
        alloc->pwrite(n_id, n.bytes);
        return old;
    }

    n.leaf.insert(i, key, value);
    ++h->size;

    if(n.leaf.size >= leaf_node::N_KEYS)
    {
        node sibling = {};
        sibling.leaf = leaf_node {};
        sibling.type = node_type::leaf;

        uint64_t sibling_id = alloc->pnew();
        Key pkey = n.leaf.split(&sibling.leaf, n_id, sibling_id);
        if(sibling.leaf.next > 0)
        {
            node next = {};
            alloc->pread(sibling.leaf.next, next.bytes);
            next.leaf.prev = sibling_id;
            alloc->pwrite(sibling.leaf.next, next.bytes);
        }
        if(n_id == h->last_leaf)
        {
            h->last_leaf = sibling_id;
        }

        if(auto new_root = insert_inner(&ctx, pkey, sibling_id, alloc))
        {
            uint64_t new_root_id = alloc->pnew();
            new_root->inner.pointers[0] = h->root;
            alloc->pwrite(new_root_id, new_root->bytes);
            h->root = new_root_id;
            ++h->height;
        }

        alloc->pwrite(sibling_id, sibling.bytes);
    }

    alloc->pwrite(n_id, n.bytes);
    return {};
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::remove(header* h, const Key& key,
                                                     Alloc<PAGE_SIZE>* alloc)
    -> std::optional<Value>
{
    Comp comp;
    auto ctx = find_leaf(h->root, key, comp, *alloc);
    node n = {};
    auto n_id = ctx.top().first;
    ctx.pop();
    alloc->pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    if(i >= n.leaf.size || n.leaf.keys[i] != key)
    {
        return {};
    }

    Value old = n.leaf.remove(i);
    --h->size;

    if(n_id == h->root)
    {
        alloc->pwrite(n_id, n.bytes);
        return old;
    }

    node p = {};
    auto [p_id, p_i] = ctx.top();
    ctx.pop();
    alloc->pread(p_id, p.bytes);

    if(i == 0 && p_i > 0)
    {
        p.inner.keys[p_i - 1] = n.leaf.keys[0];
        alloc->pwrite(p_id, p.bytes);
    }

    if(n.leaf.size < leaf_node::N_KEYS / 2)
    {
        node left = {};
        uint64_t left_id = 0;
        if(p_i > 0)
        {
            left_id = p.inner.pointers[p_i - 1];
            alloc->pread(left_id, left.bytes);
        }

        node right = {};
        uint64_t right_id = 0;
        if(p_i < p.inner.size)
        {
            right_id = p.inner.pointers[p_i + 1];
            alloc->pread(right_id, right.bytes);
        }

        if(left_id > 0 && left.leaf.size > leaf_node::N_KEYS / 2)
        {
            left.leaf.share_to(
                &n.leaf, &p.inner.keys[p_i - 1],
                left.leaf.size
                    - std::ceil((left.leaf.size + n.leaf.size) / 2.F));
            alloc->pwrite(p_id, p.bytes);
            alloc->pwrite(left_id, left.bytes);
            alloc->pwrite(n_id, n.bytes);
        }
        else if(right_id > 0 && right.leaf.size > leaf_node::N_KEYS / 2)
        {
            n.leaf.share_from(
                &right.leaf, &p.inner.keys[p_i],
                right.leaf.size
                    - std::ceil((n.leaf.size + right.leaf.size) / 2.F));
            alloc->pwrite(p_id, p.bytes);
            alloc->pwrite(n_id, n.bytes);
            alloc->pwrite(right_id, right.bytes);
        }
        else if(left_id > 0)
        {
            left.leaf.share_from(&n.leaf, &p.inner.keys[p_i - 1], n.leaf.size);
            if(left.leaf.next > 0)
            {
                node next = {};
                alloc->pread(left.leaf.next, next.bytes);
                next.leaf.prev = left_id;
                alloc->pwrite(left.leaf.next, next.bytes);
            }
            if(n_id == h->last_leaf)
            {
                h->last_leaf = left_id;
            }

            if(auto new_root_id =
                   remove_inner(&ctx, std::move(p), p_id, p_i - 1, alloc))
            {
                alloc->pdelete(h->root);
                h->root = *new_root_id;
                --h->height;
            }

            alloc->pwrite(left_id, left.bytes);
            alloc->pdelete(n_id);
        }
        else if(right_id > 0)
        {
            n.leaf.share_from(&right.leaf, &p.inner.keys[p_i], right.leaf.size);
            if(left.leaf.next > 0)
            {
                node next = {};
                alloc->pread(n.leaf.next, next.bytes);
                next.leaf.prev = n_id;
                alloc->pwrite(n.leaf.next, next.bytes);
            }
            if(right_id == h->last_leaf)
            {
                h->last_leaf = n_id;
            }

            if(auto new_root_id =
                   remove_inner(&ctx, std::move(p), p_id, p_i, alloc))
            {
                alloc->pdelete(h->root);
                h->root = *new_root_id;
                --h->height;
            }

            alloc->pwrite(n_id, n.bytes);
            alloc->pdelete(right_id);
        }
    }
    else
    {
        alloc->pwrite(n_id, n.bytes);
    }

    return old;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
void map<Key, Value, Alloc, Comp, PAGE_SIZE>::join(header* h1, header* h2,
                                                   Alloc<PAGE_SIZE>* alloc)
{
    node other_first_leaf = {};
    alloc->pread(h2->first_leaf, other_first_leaf.bytes);

    auto other_leaves = std::make_pair(&h2->first_leaf, &h2->last_leaf);
    h1->root = join_nodes(h1->root, h2->root, &h1->height, h2->height,
                          other_first_leaf.leaf.keys[0], &h1->first_leaf,
                          &h1->last_leaf, &other_leaves, alloc);
    h1->size += h2->size;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::split(header* h1, const Key& key,
                                                    Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h2 = {};
    Comp comp;
    auto ctx = find_leaf(h1->root, key, comp, *alloc);
    h1->root = ctx.top().first;
    h2.root = alloc->pnew();
    ctx.pop();

    h2.first_leaf = h2.root;
    h2.last_leaf = h1->root == h1->last_leaf ? h2.root : h1->last_leaf;
    h2.height = 0;
    h2.size = 0;
    h1->last_leaf = h1->root;
    h1->height = 0;
    h1->size = 0;

    node l = {};
    node r = {};
    alloc->pread(h1->root, l.bytes);
    r.leaf = leaf_node {};
    r.type = node_type::leaf;

    uint32_t i = detail::lb(l.leaf, key, comp);
    uint32_t size = l.leaf.size;
    std::move(&l.leaf.keys[0] + i, &l.leaf.keys[0] + size, &r.leaf.keys[0]);
    std::move(&l.leaf.values[0] + i, &l.leaf.values[0] + size,
              &r.leaf.values[0]);
    l.leaf.size -= size - i;
    r.leaf.size += size - i;
    r.leaf.next = l.leaf.next;
    l.leaf.next = 0;
    r.leaf.prev = 0;

    if(l.leaf.size > 0)
    {
        h1->size = 1;
    }

    if(r.leaf.size > 0)
    {
        h2.size = 1;
    }

    alloc->pwrite(h1->root, l.bytes);
    alloc->pwrite(h2.root, r.bytes);

    if(l.leaf.size == 0 && l.leaf.prev > 0)
    {
        node prev = {};
        alloc->pread(l.leaf.prev, prev.bytes);
        prev.leaf.next = 0;
        alloc->pwrite(l.leaf.prev, prev.bytes);
        h1->last_leaf = l.leaf.prev;
    }

    if(r.leaf.size == 0 && r.leaf.next > 0)
    {
        node next = {};
        alloc->pread(r.leaf.next, next.bytes);
        next.leaf.prev = 0;
        alloc->pwrite(r.leaf.next, next.bytes);
        h2.first_leaf = r.leaf.next;
    }

    Key l_small = l.leaf.keys[0];
    Key r_small = r.leaf.keys[0];
    uint32_t h = 1;
    uint64_t _1 = 0;
    uint64_t _2 = 0;
    while(!ctx.empty())
    {
        auto [l2_id, i] = ctx.top();
        uint64_t r2_id = alloc->pnew();
        ctx.pop();

        node l2 = {};
        node r2 = {};
        alloc->pread(l2_id, l2.bytes);
        r2.inner = inner_node {};
        r2.type = node_type::inner;

        uint32_t size = l2.inner.size;
        std::move(&l2.inner.keys[0] + i + (i < size), &l2.inner.keys[0] + size,
                  &r2.inner.keys[0]);
        std::move(&l2.inner.pointers[0] + i + 1,
                  &l2.inner.pointers[0] + size + 1, &r2.inner.pointers[0]);
        l2.inner.size -= size - i + (i > 0);
        r2.inner.size += size - i - (i < size);

        alloc->pwrite(l2_id, l2.bytes);
        alloc->pwrite(r2_id, r2.bytes);

        if(i > 0)
        {
            l_small = l2.inner.keys[i - 1];
        }

        if(i < size)
        {
            r_small = l2.inner.keys[i];
        }

        if(i == 1)
        {
            uint32_t l2_h = h - 1;
            alloc->pdelete(l2_id);
            l2_id = l2.inner.pointers[0];
            alloc->pread(l2_id, l2.bytes);
            h1->root = join_nodes(l2_id, h1->root, &l2_h, h1->height, l_small,
                                  &_1, &h1->last_leaf, nullptr, alloc);
            h1->height = l2_h;
            h1->size = 1;
        }
        else if(i > 1)
        {
            uint32_t l2_h = h;
            h1->root = join_nodes(l2_id, h1->root, &l2_h, h1->height, l_small,
                                  &_1, &h1->last_leaf, nullptr, alloc);
            h1->height = l2_h;
            h1->size = 1;
        }
        else
        {
            alloc->pdelete(l2_id);
        }

        if(i == size - 1)
        {
            alloc->pdelete(r2_id);
            r2_id = r2.inner.pointers[r2.inner.size];
            h2.root = join_nodes(h2.root, r2_id, &h2.height, h - 1, r_small,
                                 &h2.first_leaf, &_2, nullptr, alloc);
            h2.size = 1;
        }
        else if(i < size - 1)
        {
            h2.root = join_nodes(h2.root, r2_id, &h2.height, h, r_small,
                                 &h2.first_leaf, &_2, nullptr, alloc);
            h2.size = 1;
        }
        else
        {
            alloc->pdelete(r2_id);
        }
        h++;
    }

    if(h2.height == 0)
    {
        h2.last_leaf = h2.first_leaf;
    }

    return h2;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::find(
    const header& h, const Key& key, const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    auto it = lower_bound(h, key, Comp(), alloc);
    return (it != end(h, alloc) && it->first == key) ? it : end(h, alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::begin(
    const header& h, const Alloc<PAGE_SIZE>& alloc) -> const_iterator
{
    return const_iterator(h.first_leaf, 0, alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::end(const header& h,
                                                  const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    return const_iterator(0, 0, alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::rbegin(
    const header& h, const Alloc<PAGE_SIZE>& alloc) -> const_reverse_iterator
{
    return const_reverse_iterator(h.last_leaf,
                                  std::numeric_limits<uint32_t>::max(), alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::rend(
    const header& h, const Alloc<PAGE_SIZE>& alloc) -> const_reverse_iterator
{
    return const_reverse_iterator(0, std::numeric_limits<uint32_t>::max(),
                                  alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::lower_bound(
    const header& h, const Key& key, CompareFunc comp,
    const Alloc<PAGE_SIZE>& alloc) -> const_iterator
{
    auto ctx = find_leaf(h.root, key, comp, alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);
    uint32_t i = detail::lb(n.leaf, key, comp);

    return i < n.leaf.size ? const_iterator(n_id, i, alloc)
                           : const_iterator(n.leaf.next, 0, alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::upper_bound(
    const header& h, const Key& key, CompareFunc comp,
    const Alloc<PAGE_SIZE>& alloc) -> const_iterator
{
    auto ctx = find_leaf(h.root, key, comp, alloc);
    node n = {};
    uint64_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);
    uint32_t i = detail::ub(n.leaf, key, comp);

    return i < n.leaf.size ? const_iterator(n_id, i, alloc)
                           : const_iterator(n.leaf.next, 0, alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::find_leaf(
    const header& h, const Key& key, CompareFunc comp,
    const Alloc<PAGE_SIZE>& alloc) -> context
{
    return find_leaf(h.root, key, comp, alloc);
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::insert_inner(
    context* ctx, const Key& key, uint64_t pointer, Alloc<PAGE_SIZE>* alloc)
    -> std::optional<node>
{
    auto pkey = key;
    while(!ctx->empty())
    {
        node n = {};
        auto [n_id, n_i] = ctx->top();
        ctx->pop();
        alloc->pread(n_id, n.bytes);
        n.inner.insert(n_i, pkey, pointer);

        if(n.inner.size < inner_node::N_KEYS)
        {
            alloc->pwrite(n_id, n.bytes);
            return {};
        }

        node sibling = {};
        sibling.inner = inner_node {};
        sibling.type = node_type::inner;

        pointer = alloc->pnew();
        pkey = n.inner.split(&sibling.inner);
        alloc->pwrite(n_id, n.bytes);

        alloc->pwrite(pointer, sibling.bytes);
    }

    node new_root = {};
    new_root.inner = inner_node {};
    new_root.type = node_type::inner;
    new_root.inner.insert(0, pkey, pointer);

    return new_root;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::remove_inner(
    context* ctx, node&& n, uint64_t n_id, uint32_t n_i,
    Alloc<PAGE_SIZE>* alloc) -> std::optional<uint64_t>
{
    n.inner.remove(n_i);

    while(!ctx->empty() && n.inner.size < inner_node::N_KEYS / 2 - 1)
    {
        node p = {};
        auto [p_id, p_i] = ctx->top();
        ctx->pop();
        alloc->pread(p_id, p.bytes);

        node left = {};
        uint64_t left_id = 0;
        if(p_i > 0)
        {
            left_id = p.inner.pointers[p_i - 1];
            alloc->pread(left_id, left.bytes);
        }

        node right = {};
        uint64_t right_id = 0;
        if(p_i < p.inner.size)
        {
            right_id = p.inner.pointers[p_i + 1];
            alloc->pread(right_id, right.bytes);
        }

        if(left_id > 0 && left.inner.size > inner_node::N_KEYS / 2)
        {
            left.inner.share_to(
                &n.inner, &p.inner.keys[p_i - 1],
                left.inner.size
                    - std::ceil((left.inner.size + n.inner.size) / 2.F));
            alloc->pwrite(left_id, left.bytes);
            alloc->pwrite(n_id, n.bytes);
        }
        else if(right_id > 0 && right.inner.size > inner_node::N_KEYS / 2)
        {
            n.inner.share_from(
                &right.inner, &p.inner.keys[p_i],
                right.inner.size
                    - std::ceil((n.inner.size + right.inner.size) / 2.F));
            alloc->pwrite(n_id, n.bytes);
            alloc->pwrite(right_id, right.bytes);
        }
        else if(left_id > 0)
        {
            left.inner.share_from(&n.inner, &p.inner.keys[p_i - 1],
                                  n.inner.size);
            p.inner.remove(p_i - 1);
            alloc->pwrite(left_id, left.bytes);
            alloc->pdelete(n_id);
        }
        else if(right_id > 0)
        {
            n.inner.share_from(&right.inner, &p.inner.keys[p_i],
                               right.inner.size);
            p.inner.remove(p_i);
            alloc->pwrite(n_id, n.bytes);
            alloc->pdelete(right_id);
        }

        n = std::move(p);
        n_id = p_id;
    }

    if(!ctx->empty() || n.inner.size > 0 || n.inner.pointers[0] == 0)
    {
        alloc->pwrite(n_id, n.bytes);
        return {};
    }

    return n.inner.pointers[0];
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::find_leaf(
    uint64_t n_id, const Key& key, CompareFunc comp,
    const Alloc<PAGE_SIZE>& alloc) -> context
{
    context ctx;
    node n = {};
    alloc.pread(n_id, n.bytes);

    while(n.type == node_type::inner)
    {
        uint32_t i = detail::ub(n.inner, key, comp);
        ctx.push({n_id, i});

        n_id = n.inner.pointers[i];
        alloc.pread(n_id, n.bytes);
    }

    ctx.push({n_id, 0});
    return ctx;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::find_first(
    uint64_t n_id, uint32_t max_depth, const Alloc<PAGE_SIZE>& alloc) -> context
{
    context ctx;
    node n = {};
    alloc.pread(n_id, n.bytes);

    while(max_depth > 0 && n.type == node_type::inner)
    {
        ctx.push({n_id, 0});

        n_id = n.inner.pointers[0];
        alloc.pread(n_id, n.bytes);
        --max_depth;
    }

    ctx.push({n_id, 0});
    return ctx;
}

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::find_last(
    uint64_t n_id, uint32_t max_depth, const Alloc<PAGE_SIZE>& alloc) -> context
{
    context ctx;
    node n = {};
    alloc.pread(n_id, n.bytes);

    while(max_depth > 0 && n.type == node_type::inner)
    {
        ctx.push({n_id, n.inner.size});

        n_id = n.inner.pointers[n.inner.size];
        alloc.pread(n_id, n.bytes);
        --max_depth;
    }

    ctx.push({n_id, n.leaf.size});
    return ctx;
}

/* template<typename Key, typename Value, template<size_t> typename Alloc,
 * typename Comp, size_t PAGE_SIZE> */
/* auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::join_nodes( */
/*     uint32_t n1_id, uint32_t n2_id, uint32_t* n1_height, uint32_t n2_height,
 */
/*     Key n2_smallest, uint32_t* n1_first_leaf, uint32_t* n1_last_leaf, */
/*     std::pair<uint32_t*, uint32_t*>* n2_leaves, Alloc<PAGE_SIZE>* alloc)
 */
/*     -> uint32_t */
/* { */
/*     auto is_empty = [](const node& n) */
/*     { */
/*         return (n.type == node_type::leaf && n.leaf.size == 0) */
/*                || (n.type == node_type::inner && n.inner.size == 0); */
/*     }; */

/*     node n1 = {}; */
/*     node n2 = {}; */
/*     alloc->pread(n1_id, n1.bytes); */
/*     alloc->pread(n2_id, n2.bytes); */

/*     if(is_empty(n2)) */
/*     { */
/*         if(n1.type == node_type::leaf) */
/*         { */
/*             n1.leaf.next = 0; */
/*             *n1_first_leaf = n1_id; */
/*             *n1_last_leaf = n1_id; */
/*         } */
/*         alloc->pdelete(n2_id); */
/*         return n1_id; */
/*     } */

/*     if(is_empty(n1)) */
/*     { */
/*         *n1_height = n2_height; */
/*         if(n2.type == node_type::leaf) */
/*         { */
/*             n2.leaf.next = 0; */
/*             *n1_first_leaf = n2_id; */
/*             *n1_last_leaf = n2_id; */
/*         } */
/*         else if(n2_leaves != nullptr) */
/*         { */
/*             *n1_first_leaf = *n2_leaves->first; */
/*             *n1_last_leaf = *n2_leaves->second; */
/*         } */
/*         alloc->pdelete(n1_id); */
/*         return n2_id; */
/*     } */

/*     auto run_equal = [&](node* r1_r, auto* r1, uint32_t r1_id, node* r2_r, */
/*                          auto* r2, uint32_t r2_id) */
/*     { */
/*         std::optional<uint32_t> new_root; */

/*         constexpr uint32_t max_keys = */
/*             std::remove_pointer_t<decltype(r1)>::N_KEYS; */
/*         constexpr bool is_leaf = std::is_same_v<decltype(r1), leaf_node*>; */
/*         if(r1->size + r2->size + (is_leaf ? 0 : 1) < max_keys) */
/*         { */
/*             r1->share_from(r2, &n2_smallest, r2->size); */

/*             if constexpr(is_leaf) */
/*             { */
/*                 if(r1->next > 0) */
/*                 { */
/*                     node next = {}; */
/*                     alloc->pread(r1->next, next.bytes); */
/*                     next.leaf.prev = r1_id; */
/*                     alloc->pwrite(r1->next, next.bytes); */
/*                 } */
/*                 *n1_first_leaf = r1_id; */
/*                 *n1_last_leaf = r1_id; */
/*             } */
/*             else if(n2_leaves != nullptr) */
/*             { */
/*                 node n1_last_leaf_node = {}; */
/*                 alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes); */
/*                 n1_last_leaf_node.leaf.next = *n2_leaves->first; */
/*                 alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes); */

/*                 node n2_first_leaf_node = {}; */
/*                 alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes); */
/*                 n2_first_leaf_node.leaf.prev = *n1_last_leaf; */
/*                 alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);
 */

/*                 *n1_last_leaf = *n2_leaves->second; */
/*             } */

/*             alloc->pwrite(r1_id, r1_r->bytes); */
/*             alloc->pdelete(r2_id); */
/*         } */
/*         else */
/*         { */
/*             uint32_t even = std::ceil((r1->size + r2->size) / 2.F); */
/*             if(even > r2->size) */
/*             { */
/*                 r1->share_to(r2, &n2_smallest, even - r2->size); */
/*             } */
/*             else if(even < r2->size) */
/*             { */
/*                 r1->share_from(r2, &n2_smallest, r2->size - even); */
/*             } */

/*             context empty_ctx; */
/*             if(auto new_root_node = */
/*                    insert_inner(&empty_ctx, n2_smallest, r2_id, alloc)) */
/*             { */
/*                 new_root = alloc->pnew(); */
/*                 new_root_node->inner.pointers[0] = r1_id; */
/*                 alloc->pwrite(*new_root, new_root_node->bytes); */
/*             } */

/*             if constexpr(is_leaf) */
/*             { */
/*                 r1->next = r2_id; */
/*                 r2->prev = r1_id; */
/*                 *n1_first_leaf = r1_id; */
/*                 *n1_last_leaf = r2_id; */
/*             } */
/*             else if(n2_leaves != nullptr) */
/*             { */
/*                 node n1_last_leaf_node = {}; */
/*                 alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes); */
/*                 n1_last_leaf_node.leaf.next = *n2_leaves->first; */
/*                 alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes); */

/*                 node n2_first_leaf_node = {}; */
/*                 alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes); */
/*                 n2_first_leaf_node.leaf.prev = *n1_last_leaf; */
/*                 alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);
 */

/*                 *n1_last_leaf = *n2_leaves->second; */
/*             } */

/*             alloc->pwrite(r1_id, r1_r->bytes); */
/*             alloc->pwrite(r2_id, r2_r->bytes); */
/*         } */

/*         return new_root; */
/*     }; */

/*     auto run_greater = [&](node* r1_r, auto* r1, uint32_t r1_id, node* r2_r,
 */
/*                            auto* r2, uint32_t r2_id, node* c1_r, auto* c1, */
/*                            uint32_t c1_id, node* c2_r, auto* c2, uint32_t
 * c2_id, */
/*                            context* ctx) */
/*     { */
/*         std::optional<uint32_t> new_root; */

/*         constexpr uint32_t max_keys = */
/*             std::remove_pointer_t<decltype(r2)>::N_KEYS; */
/*         constexpr bool is_leaf = std::is_same_v<decltype(r2), leaf_node*>; */
/*         if(c1->size + c2->size + r2->size + 1 < 2 * max_keys - 1) */
/*         { */
/*             uint32_t even = */
/*                 std::ceil((c1->size + c2->size + r2->size + 1) / 2.F); */
/*             if(even > c1->size) */
/*             { */
/*                 c1->share_from(c2, &r1->keys[r1->size - 1], even - c1->size,
 */
/*                                true); */
/*             } */
/*             else if(even < c1->size) */
/*             { */
/*                 c1->share_to(c2, &r1->keys[r1->size - 1], c1->size - even, */
/*                              true); */
/*             } */
/*             if(c2->size == 0 && is_leaf) */
/*             { */
/*                 r1->keys[r1->size - 1] = n2_smallest; */
/*             } */
/*             c2->share_from(r2, &n2_smallest, r2->size); */
/*             if constexpr(is_leaf) */
/*             { */
/*                 if(c2->next > 0) */
/*                 { */
/*                     node next = {}; */
/*                     alloc->pread(c2->next, next.bytes); */
/*                     next.leaf.prev = c2_id; */
/*                     alloc->pwrite(c2->next, next.bytes); */
/*                 } */
/*                 *n1_last_leaf = c2_id; */
/*             } */
/*             else if(n2_leaves != nullptr) */
/*             { */
/*                 node n1_last_leaf_node = {}; */
/*                 alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes); */
/*                 n1_last_leaf_node.leaf.next = *n2_leaves->first; */
/*                 alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes); */

/*                 node n2_first_leaf_node = {}; */
/*                 alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes); */
/*                 n2_first_leaf_node.leaf.prev = *n1_last_leaf; */
/*                 alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);
 */

/*                 *n1_last_leaf = *n2_leaves->second; */
/*             } */

/*             alloc->pwrite(r1_id, r1_r->bytes); */
/*             alloc->pwrite(c1_id, c1_r->bytes); */
/*             alloc->pwrite(c2_id, c2_r->bytes); */
/*             alloc->pdelete(r2_id); */
/*         } */
/*         else */
/*         { */
/*             if(r2->size < max_keys / 2 - 1) */
/*             { */
/*                 uint32_t even = */
/*                     std::ceil((c1->size + c2->size + r2->size) / 3.F); */
/*                 if(even > c1->size) */
/*                 { */
/*                     c1->share_from(c2, &r1->keys[r1->size - 1], even -
 * c1->size, */
/*                                    true); */
/*                 } */
/*                 else if(even < c1->size) */
/*                 { */
/*                     c1->share_to(c2, &r1->keys[r1->size - 1], c1->size -
 * even, */
/*                                  true); */
/*                 } */
/*                 c2->share_to(r2, &n2_smallest, even - r2->size); */
/*                 alloc->pwrite(r1_id, r1_r->bytes); */
/*             } */

/*             if(auto new_root_node = */
/*                    insert_inner(ctx, n2_smallest, r2_id, alloc)) */
/*             { */
/*                 new_root = alloc->pnew(); */
/*                 new_root_node->inner.pointers[0] = n1_id; */
/*                 alloc->pwrite(*new_root, new_root_node->bytes); */
/*             } */

/*             if constexpr(is_leaf) */
/*             { */
/*                 c2->next = r2_id; */
/*                 r2->prev = c2_id; */
/*                 *n1_last_leaf = r2_id; */
/*             } */
/*             else if(n2_leaves != nullptr) */
/*             { */
/*                 node n1_last_leaf_node = {}; */
/*                 alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes); */
/*                 n1_last_leaf_node.leaf.next = *n2_leaves->first; */
/*                 alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes); */

/*                 node n2_first_leaf_node = {}; */
/*                 alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes); */
/*                 n2_first_leaf_node.leaf.prev = *n1_last_leaf; */
/*                 alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);
 */

/*                 *n1_last_leaf = *n2_leaves->second; */
/*             } */

/*             alloc->pwrite(r2_id, r2_r->bytes); */
/*             alloc->pwrite(c1_id, c1_r->bytes); */
/*             alloc->pwrite(c2_id, c2_r->bytes); */
/*         } */

/*         return new_root; */
/*     }; */

/*     auto run_lesser = [&](node* r1_r, auto* r1, uint32_t r1_id, node* r2_r,
 */
/*                           auto* r2, uint32_t r2_id, node* c1_r, auto* c1, */
/*                           uint32_t c1_id, node* c2_r, auto* c2, uint32_t
 * c2_id, */
/*                           context* ctx) */
/*     { */
/*         std::optional<uint32_t> new_root; */

/*         constexpr uint32_t max_keys = */
/*             std::remove_pointer_t<decltype(r1)>::N_KEYS; */
/*         constexpr bool is_leaf = std::is_same_v<decltype(r1), leaf_node*>; */
/*         if(r1->size + c1->size + c2->size + 1 < 2 * max_keys - 1) */
/*         { */
/*             uint32_t even = */
/*                 std::ceil((r1->size + c1->size + c2->size + 1) / 2.F); */
/*             if(even > c2->size) */
/*             { */
/*                 c1->share_to(c2, &r2->keys[0], even - c2->size, true); */
/*             } */
/*             else if(even < c2->size) */
/*             { */
/*                 c1->share_from(c2, &r2->keys[0], c2->size - even, true); */
/*             } */
/*             r1->share_to(c1, &n2_smallest, r1->size); */
/*             if constexpr(is_leaf) */
/*             { */
/*                 if(c1->prev > 0) */
/*                 { */
/*                     node prev = {}; */
/*                     alloc->pread(c1->prev, prev.bytes); */
/*                     prev.leaf.next = c1_id; */
/*                     alloc->pwrite(c1->prev, prev.bytes); */
/*                 } */
/*                 *n1_first_leaf = c1_id; */
/*             } */
/*             else if(n2_leaves != nullptr) */
/*             { */
/*                 node n1_last_leaf_node = {}; */
/*                 alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes); */
/*                 n1_last_leaf_node.leaf.next = *n2_leaves->first; */
/*                 alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes); */

/*                 node n2_first_leaf_node = {}; */
/*                 alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes); */
/*                 n2_first_leaf_node.leaf.prev = *n1_last_leaf; */
/*                 alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);
 */

/*                 *n1_last_leaf = *n2_leaves->second; */
/*             } */

/*             alloc->pwrite(r2_id, r2_r->bytes); */
/*             alloc->pwrite(c1_id, c1_r->bytes); */
/*             alloc->pwrite(c2_id, c2_r->bytes); */
/*             alloc->pdelete(r1_id); */
/*         } */
/*         else */
/*         { */
/*             if(r1->size < max_keys / 2 - 1) */
/*             { */
/*                 uint32_t even = */
/*                     std::ceil((r1->size + c1->size + c2->size) / 3.F); */
/*                 if(even > c2->size) */
/*                 { */
/*                     c1->share_to(c2, &r2->keys[0], even - c2->size, true); */
/*                 } */
/*                 else if(even < c2->size) */
/*                 { */
/*                     c1->share_from(c2, &r2->keys[0], c2->size - even, true);
 */
/*                 } */
/*                 r1->share_from(c1, &n2_smallest, even - r1->size); */
/*             } */

/*             r2->pointers[0] = r1_id; */
/*             alloc->pwrite(r2_id, r2_r->bytes); */
/*             if(auto new_root_node = */
/*                    insert_inner(ctx, n2_smallest, c1_id, alloc)) */
/*             { */
/*                 new_root = alloc->pnew(); */
/*                 new_root_node->inner.pointers[0] = n2_id; */
/*                 alloc->pwrite(*new_root, new_root_node->bytes); */
/*             } */

/*             if constexpr(is_leaf) */
/*             { */
/*                 r1->next = c1_id; */
/*                 c1->prev = r1_id; */
/*                 *n1_first_leaf = r1_id; */
/*             } */
/*             else if(n2_leaves != nullptr) */
/*             { */
/*                 node n1_last_leaf_node = {}; */
/*                 alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes); */
/*                 n1_last_leaf_node.leaf.next = *n2_leaves->first; */
/*                 alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes); */

/*                 node n2_first_leaf_node = {}; */
/*                 alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes); */
/*                 n2_first_leaf_node.leaf.prev = *n1_last_leaf; */
/*                 alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);
 */

/*                 *n1_last_leaf = *n2_leaves->second; */
/*             } */

/*             alloc->pwrite(r1_id, r1_r->bytes); */
/*             alloc->pwrite(c1_id, c1_r->bytes); */
/*             alloc->pwrite(c2_id, c2_r->bytes); */
/*         } */

/*         return new_root; */
/*     }; */

/*     uint32_t diff = *n1_height > n2_height ? *n1_height - n2_height */
/*                                            : n2_height - *n1_height; */
/*     if(*n1_height == n2_height) */
/*     { */
/*         if(auto new_root = */
/*                *n1_height == 0 */
/*                    ? run_equal(&n1, &n1.leaf, n1_id, &n2, &n2.leaf, n2_id) */
/*                    : run_equal(&n1, &n1.inner, n1_id, &n2, &n2.inner, n2_id))
 */
/*         { */
/*             n1_id = *new_root; */
/*             ++*n1_height; */
/*         } */
/*     } */
/*     else if(*n1_height > n2_height) */
/*     { */
/*         auto ctx1 = find_last(n1_id, diff, *alloc); */
/*         ctx1.pop(); */
/*         node r1 = {}; */
/*         uint32_t r1_id = ctx1.top().first; */
/*         alloc->pread(r1_id, r1.bytes); */
/*         node c1 = {}; */
/*         uint32_t c1_id = r1.inner.pointers[r1.inner.size - 1]; */
/*         alloc->pread(c1_id, c1.bytes); */
/*         node c2 = {}; */
/*         uint32_t c2_id = r1.inner.pointers[r1.inner.size]; */
/*         alloc->pread(c2_id, c2.bytes); */

/*         if(auto new_root = */
/*                n2_height == 0 */
/*                    ? run_greater(&r1, &r1.inner, r1_id, &n2, &n2.leaf, n2_id,
 */
/*                                  &c1, &c1.leaf, c1_id, &c2, &c2.leaf, c2_id,
 */
/*                                  &ctx1) */
/*                    : run_greater(&r1, &r1.inner, r1_id, &n2, &n2.inner,
 * n2_id, */
/*                                  &c1, &c1.inner, c1_id, &c2, &c2.inner,
 * c2_id, */
/*                                  &ctx1)) */
/*         { */
/*             n1_id = *new_root; */
/*             ++*n1_height; */
/*         } */
/*     } */
/*     else // if(*n1_height < n2_height) */
/*     { */
/*         auto ctx2 = find_first(n2_id, diff, *alloc); */
/*         ctx2.pop(); */
/*         node r2 = {}; */
/*         uint32_t r2_id = ctx2.top().first; */
/*         alloc->pread(r2_id, r2.bytes); */
/*         node c1 = {}; */
/*         uint32_t c1_id = r2.inner.pointers[0]; */
/*         alloc->pread(c1_id, c1.bytes); */
/*         node c2 = {}; */
/*         uint32_t c2_id = r2.inner.pointers[1]; */
/*         alloc->pread(c2_id, c2.bytes); */

/*         if(auto new_root = */
/*                *n1_height == 0 */
/*                    ? run_lesser(&n1, &n1.leaf, n1_id, &r2, &r2.inner, r2_id,
 */
/*                                 &c1, &c1.leaf, c1_id, &c2, &c2.leaf, c2_id,
 */
/*                                 &ctx2) */
/*                    : run_lesser(&n1, &n1.inner, n1_id, &r2, &r2.inner, r2_id,
 */
/*                                 &c1, &c1.inner, c1_id, &c2, &c2.inner, c2_id,
 */
/*                                 &ctx2)) */
/*         { */
/*             n2_id = *new_root; */
/*             ++n2_height; */
/*         } */

/*         n1_id = n2_id; */
/*         *n1_height = n2_height; */

/*         if(n2_leaves != nullptr) */
/*         { */
/*             *n1_last_leaf = *n2_leaves->second; */
/*         } */
/*     } */

/*     return n1_id; */
/* } */

template<typename Key, typename Value, template<size_t> typename Alloc,
         typename Comp, size_t PAGE_SIZE>
auto map<Key, Value, Alloc, Comp, PAGE_SIZE>::join_nodes(
    uint64_t n1_id, uint64_t n2_id, uint32_t* n1_height, uint32_t n2_height,
    Key n2_smallest, uint64_t* n1_first_leaf, uint64_t* n1_last_leaf,
    std::pair<uint64_t*, uint64_t*>* n2_leaves, Alloc<PAGE_SIZE>* alloc)
    -> uint64_t
{
    auto is_empty = [](const node& n)
    {
        return (n.type == node_type::leaf && n.leaf.size == 0)
               || (n.type == node_type::inner && n.inner.size == 0);
    };

    node n1 = {};
    node n2 = {};
    alloc->pread(n1_id, n1.bytes);
    alloc->pread(n2_id, n2.bytes);

    if(is_empty(n2))
    {
        if(n1.type == node_type::leaf)
        {
            n1.leaf.next = 0;
            *n1_first_leaf = n1_id;
            *n1_last_leaf = n1_id;
        }
        alloc->pdelete(n2_id);
        return n1_id;
    }

    if(is_empty(n1))
    {
        *n1_height = n2_height;
        if(n2.type == node_type::leaf)
        {
            n2.leaf.next = 0;
            *n1_first_leaf = n2_id;
            *n1_last_leaf = n2_id;
        }
        else if(n2_leaves != nullptr)
        {
            *n1_first_leaf = *n2_leaves->first;
            *n1_last_leaf = *n2_leaves->second;
        }
        alloc->pdelete(n1_id);
        return n2_id;
    }

    auto run_equal = [&](node* r1_r, auto* r1, uint64_t r1_id, node* r2_r,
                         auto* r2, uint64_t r2_id)
    {
        std::optional<uint64_t> new_root;

        constexpr uint32_t max_keys =
            std::remove_pointer_t<decltype(r1)>::N_KEYS;
        constexpr bool is_leaf = std::is_same_v<decltype(r1), leaf_node*>;
        if(r1->size + r2->size + (is_leaf ? 0 : 1) < max_keys)
        {
            r1->share_from(r2, &n2_smallest, r2->size);

            if constexpr(is_leaf)
            {
                if(r1->next > 0)
                {
                    node next = {};
                    alloc->pread(r1->next, next.bytes);
                    next.leaf.prev = r1_id;
                    alloc->pwrite(r1->next, next.bytes);
                }
                *n1_first_leaf = r1_id;
                *n1_last_leaf = r1_id;
            }
            else if(n2_leaves != nullptr)
            {
                node n1_last_leaf_node = {};
                alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes);
                n1_last_leaf_node.leaf.next = *n2_leaves->first;
                alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes);

                node n2_first_leaf_node = {};
                alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes);
                n2_first_leaf_node.leaf.prev = *n1_last_leaf;
                alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);

                *n1_last_leaf = *n2_leaves->second;
            }

            alloc->pwrite(r1_id, r1_r->bytes);
            alloc->pdelete(r2_id);
        }
        else
        {
            uint32_t even = std::ceil((r1->size + r2->size) / 2.F);
            if(even > r2->size)
            {
                r1->share_to(r2, &n2_smallest, even - r2->size);
            }
            else if(even < r2->size)
            {
                r1->share_from(r2, &n2_smallest, r2->size - even);
            }

            context empty_ctx;
            if(auto new_root_node =
                   insert_inner(&empty_ctx, n2_smallest, r2_id, alloc))
            {
                new_root = alloc->pnew();
                new_root_node->inner.pointers[0] = r1_id;
                alloc->pwrite(*new_root, new_root_node->bytes);
            }

            if constexpr(is_leaf)
            {
                r1->next = r2_id;
                r2->prev = r1_id;
                *n1_first_leaf = r1_id;
                *n1_last_leaf = r2_id;
            }
            else if(n2_leaves != nullptr)
            {
                node n1_last_leaf_node = {};
                alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes);
                n1_last_leaf_node.leaf.next = *n2_leaves->first;
                alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes);

                node n2_first_leaf_node = {};
                alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes);
                n2_first_leaf_node.leaf.prev = *n1_last_leaf;
                alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);

                *n1_last_leaf = *n2_leaves->second;
            }

            alloc->pwrite(r1_id, r1_r->bytes);
            alloc->pwrite(r2_id, r2_r->bytes);
        }

        return new_root;
    };

    auto run_greater = [&](node* r1_r, auto* r1, uint64_t r1_id, node* r2_r,
                           auto* r2, uint64_t r2_id, node* c2_r, auto* c2,
                           uint64_t c2_id, context* ctx)
    {
        std::optional<uint64_t> new_root;

        constexpr uint32_t max_keys =
            std::remove_pointer_t<decltype(r2)>::N_KEYS;
        constexpr bool is_leaf = std::is_same_v<decltype(r2), leaf_node*>;
        if(c2->size + r2->size + (is_leaf ? 0 : 1) < max_keys)
        {
            c2->share_from(r2, &n2_smallest, r2->size);

            if constexpr(is_leaf)
            {
                if(c2->next > 0)
                {
                    node next = {};
                    alloc->pread(c2->next, next.bytes);
                    next.leaf.prev = c2_id;
                    alloc->pwrite(c2->next, next.bytes);
                }
                *n1_last_leaf = c2_id;
            }
            else if(n2_leaves != nullptr)
            {
                node n1_last_leaf_node = {};
                alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes);
                n1_last_leaf_node.leaf.next = *n2_leaves->first;
                alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes);

                node n2_first_leaf_node = {};
                alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes);
                n2_first_leaf_node.leaf.prev = *n1_last_leaf;
                alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);

                *n1_last_leaf = *n2_leaves->second;
            }

            alloc->pwrite(r1_id, r1_r->bytes);
            alloc->pwrite(c2_id, c2_r->bytes);
            alloc->pdelete(r2_id);
        }
        else
        {
            if(r2->size < max_keys / 2 - 1)
            {
                uint32_t even = std::ceil((c2->size + r2->size) / 2.F);
                if(even > r2->size)
                {
                    c2->share_to(r2, &n2_smallest, even - r2->size);
                }
                else if(even < r2->size)
                {
                    c2->share_from(r2, &n2_smallest, r2->size - even);
                }

                alloc->pwrite(r1_id, r1_r->bytes);
            }

            if(auto new_root_node =
                   insert_inner(ctx, n2_smallest, r2_id, alloc))
            {
                new_root = alloc->pnew();
                new_root_node->inner.pointers[0] = n1_id;
                alloc->pwrite(*new_root, new_root_node->bytes);
            }

            if constexpr(is_leaf)
            {
                c2->next = r2_id;
                r2->prev = c2_id;
                *n1_last_leaf = r2_id;
            }
            else if(n2_leaves != nullptr)
            {
                node n1_last_leaf_node = {};
                alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes);
                n1_last_leaf_node.leaf.next = *n2_leaves->first;
                alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes);

                node n2_first_leaf_node = {};
                alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes);
                n2_first_leaf_node.leaf.prev = *n1_last_leaf;
                alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);

                *n1_last_leaf = *n2_leaves->second;
            }

            alloc->pwrite(r2_id, r2_r->bytes);
            alloc->pwrite(c2_id, c2_r->bytes);
        }

        return new_root;
    };

    auto run_lesser = [&](node* r1_r, auto* r1, uint64_t r1_id, node* r2_r,
                          auto* r2, uint64_t r2_id, node* c1_r, auto* c1,
                          uint64_t c1_id, context* ctx)
    {
        std::optional<uint64_t> new_root;

        constexpr uint32_t max_keys =
            std::remove_pointer_t<decltype(r1)>::N_KEYS;
        constexpr bool is_leaf = std::is_same_v<decltype(r1), leaf_node*>;
        if(r1->size + c1->size + (is_leaf ? 0 : 1) < max_keys)
        {
            r1->share_to(c1, &n2_smallest, r1->size);

            if constexpr(is_leaf)
            {
                if(c1->prev > 0)
                {
                    node prev = {};
                    alloc->pread(c1->prev, prev.bytes);
                    prev.leaf.next = c1_id;
                    alloc->pwrite(c1->prev, prev.bytes);
                }
                *n1_first_leaf = c1_id;
            }
            else if(n2_leaves != nullptr)
            {
                node n1_last_leaf_node = {};
                alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes);
                n1_last_leaf_node.leaf.next = *n2_leaves->first;
                alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes);

                node n2_first_leaf_node = {};
                alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes);
                n2_first_leaf_node.leaf.prev = *n1_last_leaf;
                alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);

                *n1_last_leaf = *n2_leaves->second;
            }

            alloc->pwrite(r2_id, r2_r->bytes);
            alloc->pwrite(c1_id, c1_r->bytes);
            alloc->pdelete(r1_id);
        }
        else
        {
            if(r1->size < max_keys / 2 - 1)
            {
                uint32_t even = std::ceil((r1->size + c1->size) / 2.F);
                if(even > c1->size)
                {
                    r1->share_to(c1, &n2_smallest, even - c1->size);
                }
                else if(even < c1->size)
                {
                    r1->share_from(c1, &n2_smallest, c1->size - even);
                }
            }

            r2->pointers[0] = r1_id;
            alloc->pwrite(r2_id, r2_r->bytes);
            if(auto new_root_node =
                   insert_inner(ctx, n2_smallest, c1_id, alloc))
            {
                new_root = alloc->pnew();
                new_root_node->inner.pointers[0] = n2_id;
                alloc->pwrite(*new_root, new_root_node->bytes);
            }

            if constexpr(is_leaf)
            {
                r1->next = c1_id;
                c1->prev = r1_id;
                *n1_first_leaf = r1_id;
            }
            else if(n2_leaves != nullptr)
            {
                node n1_last_leaf_node = {};
                alloc->pread(*n1_last_leaf, n1_last_leaf_node.bytes);
                n1_last_leaf_node.leaf.next = *n2_leaves->first;
                alloc->pwrite(*n1_last_leaf, n1_last_leaf_node.bytes);

                node n2_first_leaf_node = {};
                alloc->pread(*n2_leaves->first, n2_first_leaf_node.bytes);
                n2_first_leaf_node.leaf.prev = *n1_last_leaf;
                alloc->pwrite(*n2_leaves->first, n2_first_leaf_node.bytes);

                *n1_last_leaf = *n2_leaves->second;
            }

            alloc->pwrite(r1_id, r1_r->bytes);
            alloc->pwrite(c1_id, c1_r->bytes);
        }

        return new_root;
    };

    uint32_t diff = *n1_height > n2_height ? *n1_height - n2_height
                                           : n2_height - *n1_height;
    if(*n1_height == n2_height)
    {
        if(auto new_root =
               *n1_height == 0
                   ? run_equal(&n1, &n1.leaf, n1_id, &n2, &n2.leaf, n2_id)
                   : run_equal(&n1, &n1.inner, n1_id, &n2, &n2.inner, n2_id))
        {
            n1_id = *new_root;
            ++*n1_height;
        }
    }
    else if(*n1_height > n2_height)
    {
        auto ctx1 = find_last(n1_id, diff, *alloc);
        ctx1.pop();
        node r1 = {};
        uint64_t r1_id = ctx1.top().first;
        alloc->pread(r1_id, r1.bytes);
        node c2 = {};
        uint64_t c2_id = r1.inner.pointers[r1.inner.size];
        alloc->pread(c2_id, c2.bytes);

        if(auto new_root =
               n2_height == 0
                   ? run_greater(&r1, &r1.inner, r1_id, &n2, &n2.leaf, n2_id,
                                 &c2, &c2.leaf, c2_id, &ctx1)
                   : run_greater(&r1, &r1.inner, r1_id, &n2, &n2.inner, n2_id,
                                 &c2, &c2.inner, c2_id, &ctx1))
        {
            n1_id = *new_root;
            ++*n1_height;
        }
    }
    else // if(*n1_height < n2_height)
    {
        auto ctx2 = find_first(n2_id, diff, *alloc);
        ctx2.pop();
        node r2 = {};
        uint64_t r2_id = ctx2.top().first;
        alloc->pread(r2_id, r2.bytes);
        node c1 = {};
        uint64_t c1_id = r2.inner.pointers[0];
        alloc->pread(c1_id, c1.bytes);

        if(auto new_root =
               *n1_height == 0
                   ? run_lesser(&n1, &n1.leaf, n1_id, &r2, &r2.inner, r2_id,
                                &c1, &c1.leaf, c1_id, &ctx2)
                   : run_lesser(&n1, &n1.inner, n1_id, &r2, &r2.inner, r2_id,
                                &c1, &c1.inner, c1_id, &ctx2))
        {
            n2_id = *new_root;
            ++n2_height;
        }

        n1_id = n2_id;
        *n1_height = n2_height;

        if(n2_leaves != nullptr)
        {
            *n1_last_leaf = *n2_leaves->second;
        }
    }

    return n1_id;
}

} // namespace disk
